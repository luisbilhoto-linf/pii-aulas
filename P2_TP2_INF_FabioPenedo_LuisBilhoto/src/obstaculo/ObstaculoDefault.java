package obstaculo;

import java.awt.*;

import cenario.Cenario;
import cenario.Civil;
import cenario.Soldado;
import prof.jogos2D.image.ComponenteAnimado;
import prof.jogos2D.image.ComponenteMultiAnimado;

/**
 * Classe que representa um obst�culo e define o comportamento b�sico de todos
 * os obst�culo.
 */
public abstract class ObstaculoDefault implements Obstaculo {

	/**
	 * A figura deve ter pelo menos uma anima��o: a de estar parado, consoante o
	 * tipo de obst�culo pode ter mais que uma
	 */
	private ComponenteMultiAnimado visual;

	/**
	 * Para os obst�culos que podem terminar o n�vel, precisam de uma imagem final
	 */
	private ComponenteAnimado imagemFinal;

	/** A coordenada onde est� no cen�rio */
	private Point posicao;

	/** Constantes para o estado do obst�culo */
	protected static final int PARADO = 0;

	/** Estado inicial do obst�culo */
	private int status = PARADO;

	/** o cen�rio onde est� colocado */
	private Cenario cenario;

	/**
	 * Construtor do obst�culo
	 * 
	 * @param visual o aspeto do obst�culo. Deve ter, pelo menos, uma anima��o: a de
	 *               estar parado
	 */
	public ObstaculoDefault(ComponenteMultiAnimado visual) {
		this(visual, null);
	}

	/**
	 * Construtor do obst�culo
	 * 
	 * @param visual o aspeto do obst�culo. Deve ter, pelo menos, uma anima��o: a de
	 *               estar parado
	 * @param imgFim imagem do final de n�vel, para os obst�culos que podem terminar
	 *               um n�vel
	 */
	public ObstaculoDefault(ComponenteMultiAnimado visual, ComponenteAnimado imgFim) {
		this.visual = visual;
		imagemFinal = imgFim;
	}

	@Override
	public void atualizar() { }

	@Override
	public boolean ePassavel(Soldado s) {
		return false;
	}

	@Override
	public boolean eTransparente() {
		return true;
	}

	@Override
	public void ativar() { }

	@Override
	public boolean podeOcupar(Civil c) {
		return c != null && cenario.getCivil(posicao) == null;
	}

	@Override
	public void entrar(Soldado op) { }

	@Override
	public void entrar(Civil c) { }

	@Override
	public void sair(Soldado op) { }

	@Override
	public void sair(Civil c) { }

	/**
	 * Retorna o estado do obst�culo
	 * 
	 * @return o estado do obst�culo
	 */
	protected int getStatus() {
		return status;
	}

	/**
	 * Define o estado do obst�culo
	 * 
	 * @param status o novo estado do obst�culo
	 */
	protected void setStatus(int status) {
		this.status = status;
	}

	@Override
	public Cenario getCenario() {
		return cenario;
	}

	@Override
	public void setCenario(Cenario arm) {
		cenario = arm;
	}

	@Override
	public ComponenteMultiAnimado getVisual() {
		return visual;
	}

	@Override
	public void setVisual(ComponenteMultiAnimado cv) {
		visual = cv;
		visual.setPosicaoCentro(cenario.getEcran(posicao));
	}

	@Override
	public ComponenteAnimado getImagemFinal() {
		return imagemFinal;
	}

	@Override
	public void desenhar(Graphics2D g) {
		visual.desenhar(g);
	}

	@Override
	public Point getPosicao() {
		return posicao;
	}

	@Override
	public void setPosicao(Point pos) {
		posicao = pos;
		Point pecran = cenario.getEcran(pos);
		visual.setPosicaoCentro(pecran);
	}
}
