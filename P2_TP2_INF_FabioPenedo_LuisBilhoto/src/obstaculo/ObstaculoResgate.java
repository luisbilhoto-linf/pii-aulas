package obstaculo;

import java.awt.Point;

import cenario.Cenario;
import cenario.Civil;
import prof.jogos2D.image.ComponenteAnimado;
import prof.jogos2D.image.ComponenteMultiAnimado;

public class ObstaculoResgate extends ObstaculoDefault {
	private static final int RECEBER = 10;
	private boolean resgateOcupado = false;
	private int numAceites;

	/**
	 * Construtor do obst�culo, para quando � um do tipo resgate
	 * 
	 * @param numCiv n�mero de civis que devem ser resgatados
	 * @param visual o aspeto do obst�culo. Deve ter, pelo menos, duas anima��es: 0
	 *               = a de estar parado 1 = a de receber civis
	 * @param imgFim imagem final, quando a zona de resgate estiver completa
	 */
	public ObstaculoResgate(int numCiv, ComponenteMultiAnimado visual, ComponenteAnimado imgFim) {
		super(visual, imgFim);
		numAceites = numCiv;
	}

	@Override
	public void atualizar() {
		atualizarResgate();
	}

	/** Processa um ciclo de atualiza��o de uma zona de resgate */
	private void atualizarResgate() {
		Cenario cenario = getCenario();
		Point posicao = getPosicao();

		if (getStatus() == RECEBER) {
			ComponenteMultiAnimado visual = getVisual();
			if (visual.numCiclosFeitos() < 1)
				return;

			if (!resgateOcupado) {
				setStatus(PARADO);
				visual.setAnim(0);
				visual.reset();
			} else {
				ComponenteAnimado imagemFinal = getImagemFinal();
				imagemFinal.setPosicaoCentro(visual.getPosicaoCentro());
				cenario.removerObstaculo(posicao);
				cenario.iniciaFimNivel(true, imagemFinal);
			}
		}

		// se tiver algum civil, tem de o remover
		cenario.removerCivil(posicao);

		// ver se j� os recebeu a todos
		if (numAceites == 0)
			return;

		// ver se ainda h� civis para "puxar"
		Point[] ondeVer = { new Point(posicao.x + 1, posicao.y), new Point(posicao.x - 1, posicao.y),
				new Point(posicao.x, posicao.y + 1), new Point(posicao.x, posicao.y - 1) };

		for (Point p : ondeVer) {
			Civil c = cenario.getCivil(p);
			if (c != null) {
				c.deslocar(posicao.x - p.x, posicao.y - p.y);
				break;
			}
		}
	}

	@Override
	public boolean eTransparente() {
		return false;
	}

	@Override
	public void entrar(Civil c) {
		ComponenteMultiAnimado visual = getVisual();

		if (numAceites > 0)
			numAceites--;
		if (numAceites == 0)
			resgateOcupado = true;
		setStatus(RECEBER);
		visual.setAnim(1);
		visual.reset();
	}
}
