package obstaculo;

import prof.jogos2D.image.ComponenteMultiAnimado;

public class ObstaculoBarricada extends ObstaculoDefault {
	private static final int DESTRUIR = 20;
	private int nAtual, nMax;

	/**
	 * Construtor do obst�culo, para quando � um do tipo barricada
	 * 
	 * @param visual o aspeto do obst�culo. Deve ter, pelo menos, uma anima��o: a de
	 *               estar parado
	 */
	public ObstaculoBarricada(int nivel, ComponenteMultiAnimado visual) {
		super(visual);
		nMax = nAtual = nivel;
	}

	@Override
	public void atualizar() {
		atualizarBarricada();
	}

	/** Processa um ciclo de atualiza��o de uma barricada */
	private void atualizarBarricada() {
		if (getStatus() == DESTRUIR) {
			if (getVisual().numCiclosFeitos() < 1)
				return;

			if (nAtual <= 0)
				getCenario().removerObstaculo(getPosicao());
			else if (calcAnim() % 2 == 1)
				mudarAnim(2);
			setStatus(PARADO);
		}
	}

	@Override
	public boolean eTransparente() {
		return false;
	}

	@Override
	public void ativar() {
		nAtual--;
		if (nAtual >= 0)
			mudarAnim(calcAnim());
		setStatus(DESTRUIR);
	}

	/**
	 * Muda a anima��o de destrui��o da barricada
	 * 
	 * @param a a nova anima��o a utilizar
	 */
	private void mudarAnim(int a) {
		ComponenteMultiAnimado v = getVisual();
		if (v.getAnim() < a) {
			v.setAnim(a);
			v.reset();
		}
	}

	/** Calcula o n�mero do conjunto de sprites a ser usado */
	private final int calcAnim() {
		ComponenteMultiAnimado v = getVisual();
		return (int) ((v.totalFrames() - (Math.floor(nAtual * v.totalFrames()) / nMax)) - 1);
	}
}
