package obstaculo;

import cenario.Civil;
import cenario.Soldado;
import prof.jogos2D.image.ComponenteAnimado;
import prof.jogos2D.image.ComponenteMultiAnimado;

public class ObstaculoAlternavel extends ObstaculoDefault {
	private static final int ATIVO = 100;
	private int tempo, tempoOn, tempoOff;
	private boolean alternaOcupado;

	/**
	 * Construtor do obst�culo, para quando � um do tipo altern�vel
	 * 
	 * @param on       se est� ativo
	 * @param tempoOn  durante quantos ciclos fica ativo
	 * @param tempoOff durante quantos ciclos fica inativo
	 * @param visual   visual o aspeto do obst�culo. Deve ter, pelo menos, duas
	 *                 anima��es: 0 = a de estar inativo 1 = a de estar ativo
	 * @param imgFim   imagem final, se uma pessoa estiver no obst�culo quando est�
	 *                 ativo, perde o n�vel e passa a anima��o final
	 */
	public ObstaculoAlternavel(boolean on, int tempoOn, int tempoOff, ComponenteMultiAnimado visual,
			ComponenteAnimado imgFim) {
		super(visual, imgFim);
		setStatusAtivo(on ? ATIVO : PARADO);
		this.tempoOn = tempoOn;
		this.tempoOff = tempoOff;
		tempo = on ? tempoOn : tempoOff;
	}

	/**
	 * Define o estado ativo ou desativo e ajusta a anima��o de acordo
	 * 
	 * @param status o novo status
	 */
	private void setStatusAtivo(int status) {
		ComponenteMultiAnimado visual = getVisual();

		setStatus(status);
		if (status == ATIVO) {
			visual.setAnim(1);
			tempo = tempoOn;
		} else {
			visual.setAnim(0);
			tempo = tempoOff;
		}
		visual.reset();
	}

	@Override
	public void atualizar() {
		atualizarAlternavel();
	}

	/** Processa um ciclo de atualiza��o de um obstaculo altern�vel */
	private void atualizarAlternavel() {
		int status = getStatus();

		tempo--;
		if (tempo <= 0)
			setStatusAtivo(status == ATIVO ? PARADO : ATIVO);
		if (status == ATIVO && alternaOcupado) {
			ComponenteAnimado imagemFinal = getImagemFinal();
			imagemFinal.setPosicaoCentro(getVisual().getPosicaoCentro());
			getCenario().iniciaFimNivel(false, imagemFinal);
		}
	}

	@Override
	public boolean ePassavel(Soldado s) {
		return true;
	}

	@Override
	public boolean eTransparente() {
		return getStatus() != ATIVO;
	}

	@Override
	public void entrar(Soldado op) {
		alternaOcupado = true;
	}

	@Override
	public void entrar(Civil c) {
		alternaOcupado = true;
	}

	@Override
	public void sair(Soldado op) {
		alternaOcupado = false;
	}

	@Override
	public void sair(Civil c) {
		alternaOcupado = false;
	}
}
