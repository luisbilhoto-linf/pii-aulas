package obstaculo;

import cenario.Civil;
import cenario.Soldado;
import prof.jogos2D.image.ComponenteAnimado;
import prof.jogos2D.image.ComponenteMultiAnimado;

public class ObstaculoMina extends ObstaculoDefault {

	/**
	 * Construtor do obst�culo, para quando � um do tipo mina
	 * 
	 * @param visual o aspeto do obst�culo. Deve ter, pelo menos, uma anima��o: a de
	 *               estar parado
	 * @param imgFim imagem do final de n�vel, para os obst�culos que podem terminar
	 *               um n�vel
	 */
	public ObstaculoMina(ComponenteMultiAnimado visual, ComponenteAnimado imgFim) {
		super(visual, imgFim);
	}

	@Override
	public boolean ePassavel(Soldado s) {
		return true;
	}

	@Override
	public void entrar(Soldado op) {
		explodirMina();
	}

	@Override
	public void entrar(Civil c) {
		explodirMina();
	}

	/** Explode a mina */
	private void explodirMina() {
		ComponenteAnimado imagemFinal = getImagemFinal();
		imagemFinal.setPosicaoCentro(getVisual().getPosicaoCentro());
		getCenario().iniciaFimNivel(false, imagemFinal);
	}
}
