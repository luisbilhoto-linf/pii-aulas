package obstaculo;

import cenario.Civil;
import prof.jogos2D.image.ComponenteMultiAnimado;

public class ObstaculoParede extends ObstaculoDefault {

	/**
	 * Construtor do obst�culo, para quando � um do tipo parede
	 * 
	 * @param visual o aspeto do obst�culo. Deve ter, pelo menos, uma anima��o: a de
	 *               estar parado
	 */
	public ObstaculoParede(ComponenteMultiAnimado visual) {
		super(visual);
	}

	@Override
	public boolean eTransparente() {
		return false;
	}

	@Override
	public boolean podeOcupar(Civil c) {
		return false;
	}
}
