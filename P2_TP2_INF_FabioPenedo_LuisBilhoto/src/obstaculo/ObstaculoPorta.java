package obstaculo;

import cenario.Soldado;
import prof.jogos2D.image.ComponenteMultiAnimado;

public class ObstaculoPorta extends ObstaculoDefault {
	private static final int ABRIR = 30;
	private static final int FECHAR = 35;
	private boolean aberta;

	/**
	 * Construtor do obst�culo, para quando � um do tipo porta
	 * 
	 * @param visual o aspeto do obst�culo. Deve ter, pelo menos, uma anima��o: a de
	 *               estar parado
	 */
	public ObstaculoPorta(boolean aberta, ComponenteMultiAnimado visual) {
		super(visual);
		this.aberta = aberta;
	}

	@Override
	public void atualizar() {
		atualizarPorta();
	}

	/** Processa um ciclo de atualiza��o de uma porta */
	private void atualizarPorta() {
		int status = getStatus();

		if (status != PARADO) {
			ComponenteMultiAnimado visual = getVisual();
			if (visual.numCiclosFeitos() < 1)
				return;

			if (status == ABRIR)
				visual.setAnim(2);
			else if (status == FECHAR)
				visual.setAnim(0);
			setStatus(PARADO);
			visual.reset();
		}
	}

	@Override
	public boolean ePassavel(Soldado s) {
		return aberta;
	}

	@Override
	public boolean eTransparente() {
		return aberta;
	}

	@Override
	public void ativar() {
		// N�o deixa abrir/fechar porta com civis dentro
		if (getCenario().getCivil(getPosicao()) != null)
			return;

		ComponenteMultiAnimado visual = getVisual();
		if (!aberta) {
			visual.setAnim(1);
			setStatus(ABRIR);
		} else {
			visual.setAnim(3);
			setStatus(FECHAR);
		}
		aberta = !aberta;
		visual.reset();
	}
}
