package obstaculo;

import cenario.Civil;
import cenario.Soldado;
import prof.jogos2D.image.ComponenteAnimado;
import prof.jogos2D.image.ComponenteMultiAnimado;

public class ObstaculoMina2 extends ObstaculoMina {
	private boolean ativa;

	public ObstaculoMina2(boolean ativa, ComponenteMultiAnimado visual, ComponenteAnimado imgFim) {
		super(visual, imgFim);
		this.ativa = ativa;
	}

	@Override
	public void ativar() {
		ComponenteMultiAnimado visual = getVisual();

		if (ativa)
			visual.setAnim(1);
		else
			visual.setAnim(0);
		ativa = !ativa;
		visual.reset();
	}

	@Override
	public void entrar(Soldado op) {
		if (ativa)
			super.entrar(op);
	}

	@Override
	public void entrar(Civil c) {
		if (ativa)
			super.entrar(c);
	}
}
