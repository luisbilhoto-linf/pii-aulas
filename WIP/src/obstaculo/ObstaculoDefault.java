package obstaculo;

import java.awt.*;

import cenario.Cenario;
import cenario.Civil;
import cenario.Soldado;
import prof.jogos2D.image.ComponenteAnimado;
import prof.jogos2D.image.ComponenteMultiAnimado;

public abstract class ObstaculoDefault implements ObstaculoInterface {
	private ComponenteMultiAnimado visual;
	private ComponenteAnimado imagemFinal;
	private Point posicao;
	protected static final int PARADO = 0;
	private int status = PARADO;
	private Cenario cenario;

	public ObstaculoDefault(ComponenteMultiAnimado visual) {
		this(visual, null);
	}

	public ObstaculoDefault(ComponenteMultiAnimado visual, ComponenteAnimado imgFim) {
		this.visual = visual;
		imagemFinal = imgFim;
	}

	@Override
	public void atualizar() {
	}

	@Override
	public boolean ePassavel(Soldado s) {
		return false;
	}

	@Override
	public boolean eTransparente() {
		return true;
	}

	@Override
	public void ativar() {
	}

	@Override
	public boolean podeOcupar(Civil c) {
		return c != null && cenario.getCivil(posicao) == null;
	}

	@Override
	public void entrar(Soldado op) {
	}

	@Override
	public void entrar(Civil c) {
	}

	@Override
	public void sair(Soldado op) {
	}

	@Override
	public void sair(Civil c) {
	}

	@Override
	public Cenario getCenario() {
		return cenario;
	}

	@Override
	public void setCenario(Cenario arm) {
		cenario = arm;
	}

	@Override
	public ComponenteMultiAnimado getVisual() {
		return visual;
	}

	@Override
	public void setVisual(ComponenteMultiAnimado cv) {
		visual = cv;
		visual.setPosicaoCentro(cenario.getEcran(posicao));
	}
	
	@Override
	public int getStatus() {
		return status;
	}

	@Override
	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public ComponenteAnimado getImagemFinal() {
		return imagemFinal;
	}

	@Override
	public void setImagemFinal(ComponenteAnimado imagemFinal) {
		this.imagemFinal = imagemFinal;
	}

	@Override
	public void desenhar(Graphics2D g) {
		visual.desenhar(g);
	}

	@Override
	public Point getPosicao() {
		return posicao;
	}

	@Override
	public void setPosicao(Point pos) {
		posicao = pos;
		Point pecran = cenario.getEcran(pos);
		visual.setPosicaoCentro(pecran);
	}
}
