package obstaculo;

import cenario.Civil;
import cenario.Soldado;
import prof.jogos2D.image.ComponenteAnimado;
import prof.jogos2D.image.ComponenteMultiAnimado;

public class Alternavel extends ObstaculoDefault {
	private static final int ATIVO = 100;
	private int tempo, tempoOn, tempoOff;
	private boolean alternaOcupado;

	public Alternavel(boolean on, int tempoOn, int tempoOff, ComponenteMultiAnimado visual, ComponenteAnimado imgFim) {
		super(visual, imgFim);
		setStatusAtivo(on ? ATIVO : PARADO);
		tempo = on ? tempoOn : tempoOff;
		this.tempoOn = tempoOn;
		this.tempoOff = tempoOff;
	}

	@Override
	public boolean ePassavel(Soldado s) {
		return true;
	}

	@Override
	public boolean eTransparente() {
		if (getStatus() != ATIVO)
			return true;
		return false;
	}

	private void setStatusAtivo(int status) {
		ComponenteMultiAnimado visual = getVisual();

		setStatus(status);
		if (status == ATIVO) {
			visual.setAnim(1);
			visual.reset();
			tempo = tempoOn;
		} else {
			visual.setAnim(0);
			visual.reset();
			tempo = tempoOff;
		}
	}

	@Override
	public void atualizar() {
		tempo--;
		int status = getStatus();
		if (tempo <= 0)
			setStatusAtivo(status == ATIVO ? PARADO : ATIVO);
		if (status == ATIVO && alternaOcupado) {
			ComponenteAnimado imagemFinal = getImagemFinal();
			imagemFinal.setPosicaoCentro(getVisual().getPosicaoCentro());
			getCenario().iniciaFimNivel(false, imagemFinal);
		}
	}

	@Override
	public void entrar(Soldado op) {
		alternaOcupado = true;
	}

	@Override
	public void entrar(Civil c) {
		alternaOcupado = true;
	}

	@Override
	public void sair(Soldado op) {
		alternaOcupado = false;
	}

	@Override
	public void sair(Civil c) {
		alternaOcupado = false;
	}
}
