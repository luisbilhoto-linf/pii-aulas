package obstaculo;

import prof.jogos2D.image.ComponenteMultiAnimado;

public class Barricada extends ObstaculoDefault {
	private int nivel;

	public Barricada(int nivel, ComponenteMultiAnimado visual) {
		super(visual);
		this.nivel = nivel;
	}
}
