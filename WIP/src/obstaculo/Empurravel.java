package obstaculo;

import java.awt.Point;

import cenario.Cenario;
import prof.jogos2D.image.ComponenteMultiAnimado;
import prof.jogos2D.util.Vector2D;

public class Empurravel extends ObstaculoDefault {

	public Empurravel(ComponenteMultiAnimado visual) {
		super(visual);
	}

	@Override
	public boolean eTransparente() {
		return false;
	}

	@Override
	public void ativar() {
		Cenario cenario = getCenario();
		Point posicao = getPosicao();
		Vector2D dir = cenario.getSoldado().getDirecao();
		int xd = (int) (posicao.x + dir.x);
		int yd = (int) (posicao.y + dir.y);
		Point dest = new Point(xd, yd);
		if (!cenario.estaOcupado(dest)) {
			cenario.moverObstaculo(this, dest);
		}
	}
}
