package obstaculo;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import cenario.Cenario;
import prof.jogos2D.image.ComponenteAnimado;
import prof.jogos2D.image.ComponenteMultiAnimado;
import prof.jogos2D.util.Vector2D;

public class SoldadoInimigo extends ObstaculoDefault {
	private ArrayList<Point> caminho = new ArrayList<Point>();
	private int velocidade = 5;
	private int posCaminho = 0;
	private int dirCaminho = 1;
	private Vector2D direcao;
	private Point destinoCentro, destino;
	private Point2D.Double posCentro;

	public SoldadoInimigo(ComponenteMultiAnimado visual, ComponenteAnimado imgFim) {
		super(visual, imgFim);
		visual.setAnim(1);
	}

	@Override
	public void atualizar() {
		atualizarSoldado();
	}

	private void atualizarSoldado() {
		moverSoldado();
		Cenario cenario = getCenario();
		Point posicao = getPosicao();
		ComponenteAnimado imagemFinal = getImagemFinal();
		if (cenario.temPessoas(posicao, direcao)) {
			Point posTiro = (Point) posicao.clone();
			posTiro.translate((int) direcao.x, (int) direcao.y);
			imagemFinal.setAngulo(direcao.getAngulo());
			imagemFinal.setPosicaoCentro(cenario.getEcran(posTiro));
			cenario.iniciaFimNivel(false, imagemFinal);
		}
	}

	@Override
	public void setPosicao(Point pos) {
		super.setPosicao(pos);
		Point pecran = getCenario().getEcran(pos);
		direcao = new Vector2D(0, 0);
		destinoCentro = pecran;
		posCentro = new Point2D.Double(pecran.x, pecran.y);
	}

	public void addPontoCaminho(Point p) {
		caminho.add(p);
	}

	public void removePontoCaminho(Point p) {
		caminho.remove(p);
	}

	public void removePontoCaminho(int idx) {
		caminho.remove(idx);
	}

	public Point getPosicaoInicio() {
		return caminho.get(0);
	}

	private int proximoIndice() {
		int nextIdx = posCaminho + dirCaminho;
		// ver se chegou a um dos extremos e volta ao in�cio
		if (dirCaminho > 0 && nextIdx >= caminho.size())
			nextIdx = 0;
		else if (dirCaminho < 0 && nextIdx < 0)
			nextIdx = caminho.size() - 1;
		return nextIdx;
	}

	private void moverSoldado() {
		posCentro.x += velocidade * direcao.x;
		posCentro.y += velocidade * direcao.y;
		ComponenteMultiAnimado visual = getVisual();
		visual.setPosicaoCentro(new Point((int) posCentro.x, (int) posCentro.y));
		if (posCentro.distanceSq(destinoCentro.x, destinoCentro.y) < 4) {
			Cenario cenario = getCenario();
			cenario.moverObstaculo(this, destino);
			int nextIdx = proximoIndice();
			Point dest = caminho.get(nextIdx);
			Point posicao = getPosicao();
			// se tem obst�culo no local para onde quer ir
			// volta para tr�s e mant�m-se na mesma posi��o
			if (cenario.getObstaculo(dest) != null) {
				dirCaminho = -dirCaminho;
				dest = posicao;
			} else {
				posCaminho = nextIdx;
			}
			destino = dest;
			destinoCentro = cenario.getEcran(dest);
			direcao = new Vector2D(dest.x - posicao.x, dest.y - posicao.y);
			direcao.normalizar();
			visual.setAngulo(direcao.getAngulo());
			visual.setPosicaoCentro(new Point((int) posCentro.x, (int) posCentro.y));
		}
	}
}
