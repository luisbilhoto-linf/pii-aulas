package obstaculo;

import prof.jogos2D.image.ComponenteMultiAnimado;

public class Porta extends ObstaculoDefault {
	private boolean aberta;

	public Porta(boolean aberta, ComponenteMultiAnimado visual) {
		super(visual);
		this.aberta = aberta;
	}
}
