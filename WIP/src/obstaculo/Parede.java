package obstaculo;

import cenario.Civil;
import prof.jogos2D.image.ComponenteMultiAnimado;

public class Parede extends ObstaculoDefault {

	public Parede(ComponenteMultiAnimado visual) {
		super(visual);
	}

	@Override
	public boolean eTransparente() {
		return false;
	}

	@Override
	public boolean podeOcupar(Civil c) {
		return false;
	}
}
