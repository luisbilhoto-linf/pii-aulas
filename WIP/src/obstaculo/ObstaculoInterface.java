package obstaculo;

import java.awt.*;

import cenario.Cenario;
import cenario.Civil;
import cenario.Soldado;
import prof.jogos2D.image.ComponenteAnimado;
import prof.jogos2D.image.ComponenteMultiAnimado;

public interface ObstaculoInterface {

	/** Processa um ciclo de atualiza��o */
	void atualizar();

	/**
	 * Indica se o soldado pode ocupar este obst�culo
	 * 
	 * @param s o soldado
	 * @return true, se o soldado pode ocupar o obst�culo
	 */
	boolean ePassavel(Soldado s);

	/**
	 * Indica se se pode ver atrav�s do obst�culo
	 * 
	 * @return true, se se puder ver atrav�s do obst�culo
	 */
	boolean eTransparente();

	/** Ativa o obst�culo */
	void ativar();

	/**
	 * Indica se o civil pode ocupar o obst�culo
	 * 
	 * @param c o civil a verificar
	 * @return se o civil pode ocupar o obst�culo
	 */
	boolean podeOcupar(Civil c);

	/**
	 * Coloca o soldado no obst�culo <br>
	 * 
	 * @param s o soldado
	 */
	void entrar(Soldado op);

	/**
	 * Coloca o civil no obst�culo. <br>
	 * 
	 * @param c o civil a colocar
	 */
	void entrar(Civil c);

	/**
	 * Remover o soldado do obst�culo. <br>
	 * 
	 * @param op o soldado a remover
	 */
	void sair(Soldado op);

	/**
	 * Remove o civil do obst�culo.
	 * 
	 * @param c o civil a remover
	 */
	void sair(Civil c);

	/**
	 * Devolve o cen�rio associado ao obst�culo
	 * 
	 * @return o cen�rio associado ao obst�culo
	 */
	Cenario getCenario();

	/**
	 * Define qual o cen�rio associado a este obst�culo. S� deve ser chamado pelo
	 * pr�prio cen�rio
	 * 
	 * @param arm o cen�rio
	 */
	void setCenario(Cenario arm);

	/**
	 * Retorna a imagem do obst�culo
	 * 
	 * @return a imagem do obst�culo
	 */
	ComponenteMultiAnimado getVisual();

	/**
	 * Define a imagem do obst�culo
	 * 
	 * @param cv a nova imagem do obst�culo
	 */
	void setVisual(ComponenteMultiAnimado cv);

	int getStatus();

	void setStatus(int status);

	ComponenteAnimado getImagemFinal();

	void setImagemFinal(ComponenteAnimado imagemFinal);

	/**
	 * Desenha o obst�culo
	 * 
	 * @param g onde desenhar
	 */
	void desenhar(Graphics2D g);

	/**
	 * Devolve a posi��o do obst�culo
	 * 
	 * @return a posi��o do obst�culo
	 */
	Point getPosicao();

	/**
	 * Define a posi��o do obst�culo. Deve ser chamada apenas pelo cen�rio
	 * 
	 * @param pos a nova posi��o
	 */
	void setPosicao(Point pos);
}
