package estrada.veiculo;

import estrada.estrada.Faixa;
import prof.jogos2D.image.ComponenteVisual;

public class Estado extends Carro {

	public Estado(ComponenteVisual img, Faixa f, int veloc, int resist) {
		super(img, f, veloc, resist);
	}

	@Override
	public boolean podeMudarFaixa(Faixa f, int x) {
		return false;
	}

	@Override
	protected void checkBloqueios() {
		return;
	}

	@Override
	protected void checkAbrandar() {
		return;
	}

	@Override
	public void setParado(boolean parado) {
		return;
	}
}
