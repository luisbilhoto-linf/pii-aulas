package estrada.veiculo;

import estrada.estrada.Faixa;
import estrada.estrada.MauPiso;
import prof.jogos2D.image.ComponenteVisual;

public class Emergencia extends Carro {

	public Emergencia(ComponenteVisual img, Faixa f, int veloc, int resist) {
		super(img, f, veloc, resist);
	}

	@Override
	public boolean podeMudarFaixa(Faixa f, int x) {
		if (getFaixa().getSentido() == f.getSentido()) {
			return super.podeMudarFaixa(f, x);
		}

		return false;
	}

	@Override
	protected void estaMauPiso(MauPiso mp) {
		super.estaMauPiso(mp);
		decResist(1);
	}

	@Override
	protected void checkBloqueios() {
		return;
	}

	@Override
	public void setParado(boolean parado) {
		return;
	}

	@Override
	protected void abranda(Veiculo c) {
		super.abranda(c);
		decResist(1);
	}
}
