package estrada.veiculo;

import java.awt.Point;

import estrada.estrada.Bloqueio;
import estrada.estrada.Faixa;
import estrada.estrada.MauPiso;
import prof.jogos2D.image.ComponenteVisual;

public class Pesado extends Carro {

	public Pesado(ComponenteVisual img, Faixa f, int veloc, int resist) {
		super(img, f, veloc, resist);
	}

	@Override
	public boolean podeMudarFaixa(Faixa f, int x) {
		int distancia = getFaixa().getEstrada().distanciaEntreFaixas(getFaixa(), f);

		return super.podeMudarFaixa(f, x) && distancia <= 1;
	}

	@Override
	protected void estaMauPiso(MauPiso mp) {
		super.estaMauPiso(mp);
		decResist(1);
	}

	@Override
	protected void bateuBloqueio(Bloqueio b, Point p) {
		decResist(2);
	}
}
