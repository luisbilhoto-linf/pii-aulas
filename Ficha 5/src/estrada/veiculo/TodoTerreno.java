package estrada.veiculo;

import estrada.estrada.Faixa;
import prof.jogos2D.image.ComponenteVisual;

public class TodoTerreno extends Carro {

	public TodoTerreno(ComponenteVisual img, Faixa f, int veloc, int resist) {
		super(img, f, veloc, resist);
	}

	@Override
	public boolean podeMudarFaixa(Faixa f, int x) {
		int distancia = getFaixa().getEstrada().distanciaEntreFaixas(getFaixa(), f);

		return super.podeMudarFaixa(f, x) && distancia <= 2;
	}

	@Override
	protected void checkMauPiso() {
		return;
	}
}
