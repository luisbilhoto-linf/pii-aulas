package menus;

import java.util.Collection;

import consola.SConsola;
import restaurante.*;
import sistema.*;

/**
 * Classe que trata dos menus do servidor
 */
public class MenuServidor {

	private SConsola consola = new SConsola("Menu do TESTCovid", 30, 30, 500, 600);
	private ComEST server;

	/**
	 * Cria a interface para o menu principal
	 * 
	 * @param s o servidor para o qual se está a cria a interface
	 */
	public MenuServidor(ComEST s) {
		server = s;
	}

	/**
	 * Menu principal da aplicação
	 */
	public void menuPrincipal() {
		char opcao = 0;

		do {
			String menu = "Menu Administrador\n\n" + "1- Ver Pedidos totais\n" + "2- Ver pedidos por restaurante\n"
					+ "\n0- Sair";
			consola.clear();
			consola.println(menu);
			opcao = consola.readChar();
			switch (opcao) {
			case '1':
				verTodosPedidos();
				break;
			case '2':
				verPedidosRestaurante();
				break;
			case '0':
				break;
			default:
				consola.println("opção inválida");
				consola.readLine();
			}
		} while (opcao != '0');

		consola.close(); // desligar o menu da central
		System.exit(0);
	}

	/**
	 * Apresenta todos os pedidos existentes no sistema
	 */
	private void verTodosPedidos() {
		verPedidos(server.getPedidos());
	}

	/**
	 * Apresenta todos os pedidos presentes na lista
	 * 
	 * @param pedidos a collection de pedidos a apresentar
	 */
	private void verPedidos(Collection<Pedido> pedidos) {
		consola.clear();
		for (Pedido p : pedidos) {
			consola.println(String.format("%6s - %-30s  %4dg  %6.2fe  %6.2f€", p.getCodigo(),
					p.getRestaurante().getNome(), p.getPeso(), p.getTarifa(), p.getPreco()));
		}
		consola.print("\nDeseja ver algum pedido em detalhe?\nInsira o código: ");
		String cod = consola.readLine();
		if (cod.length() == 6) {
			verPedido(server.getPedido(cod));
		}
	}

	/**
	 * Apresenta todos os pedidos existentes num restaurante
	 */
	private void verPedidosRestaurante() {
		consola.clear();
		for (int i = 0; i < server.getRestaurantes().size(); i++) {
			consola.println((i + 1) + ": " + server.getRestaurante(i).getNome());
		}
		consola.print("\nRestaurante: ");
		Restaurante r = server.getRestaurante(consola.readInt() - 1);
		if (r == null)
			return;

		verPedidos(r.getEncomendas());
	}

	/**
	 * Lista as informações do pedido
	 * 
	 * @param p o pedido que se pretende listar
	 */
	private void verPedido(Pedido p) {
		if (p == null) {
			consola.println("Pedido inexistente!");
			consola.readLine();
			return;
		}

		consola.clear();
		consola.println("Pedido " + p.getCodigo() + "\nRestaurante: " + p.getRestaurante().getNome() + "\n");
		consola.println(String.format("Preço         : %6.2f€", p.getPrecoTotal()));
		consola.println(String.format("Preço  pratos : %6.2f€", p.getPreco()));
		consola.println(String.format("Custo entrega : %6.2f€  (%4dg)", p.getTarifa(), p.getPeso()));

		for (Escolha e : p.getEscolhas()) {
			Prato pr = e.getPrato();
			consola.println(String.format("%-40s  %6.2f€  %4dg", pr.getNome(), pr.getPreco(), pr.getPeso()));
			for (int i = 0; i < e.getOpcoes().size(); i++) {
				Opcao o = e.getOpcoes().get(i);
				consola.println(String.format("     %-35s  %6.2f€  %4dg", o.getNome(), o.getCusto(), o.getPeso()));
			}
		}
		consola.readLine();
	}
}
