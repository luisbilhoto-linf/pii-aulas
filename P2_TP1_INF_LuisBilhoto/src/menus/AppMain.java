package menus;

import java.util.ArrayList;
import java.util.Arrays;

import restaurante.Opcao;
import restaurante.Prato;
import restaurante.Restaurante;
import sistema.ComEST;

public class AppMain {

	/**
	 * Despoleta a aplicação
	 */
	public static void main(String[] args) {
		ComEST come = new ComEST();

		// Restaurante: CantinaEST
		come.addRestaurante(
			new Restaurante("CantinaEST", "Comida caseira, elaborada com um toque de requinte.",
				new ArrayList<Prato>(Arrays.asList(
					new Prato("Rissois", "3 Rissois de bacalhau acompanhados de arroz", (float) 2.6, 300,
						new ArrayList<Opcao>(Arrays.asList(
							new Opcao("Acompanhado com arroz de tomate", (float) 0.3, 20),
							new Opcao("1 Rissol extra", (float) 0.2, 30),
							new Opcao("2 Rissois extra", (float) 0.4, 60)
						))
					),
					new Prato("Peixe à Brás", "Peixe à Brás", (float) 2.6, 400,
						new ArrayList<Opcao>(Arrays.asList(
							new Opcao("5 Azeitonas", (float) 0.2, 30),
							new Opcao("8 Azeitonas", (float) 0.3, 50),
							new Opcao("Queijo gratinado", (float) 0.4, 100)
						))
					),
					new Prato("Frango assado", "Perna de frango assado com arroz", (float) 2.6, 450,
						new ArrayList<Opcao>(Arrays.asList(
							new Opcao("Pickles", (float) 0.2, 40),
							new Opcao("Picante", (float) 0.0, 5)
						))
					)
				))
			)
		);

		// Restaurante: SoSushi
		come.addRestaurante(
			new Restaurante("SoSushi", "Comida japonesa de qualidade.",
				new ArrayList<Prato>(Arrays.asList(
					new Prato("Sushi + Sashimi mix", "Seleção de sushi e sashimi, caixa média com 18 peças", (float) 8.5, 600,
						new ArrayList<Opcao>(Arrays.asList(
							new Opcao("Caixa grande, com 42 peças", (float) 9.5, 600),
							new Opcao("Wasabi", (float) 0.3, 5),
							new Opcao("Soja", (float) 0.3, 5)
						))
					),
					new Prato("Sushi + Nigiri mix", "Seleção de sushi e nigiri, caixa média com 18 peças", (float) 10.5, 700,
						new ArrayList<Opcao>(Arrays.asList(
							new Opcao("Caixa grande, com 42 peças", (float) 14.5, 750),
							new Opcao("Wasabi", (float) 0.3, 5),
							new Opcao("Soja", (float) 0.3, 5)
						))
					),
					new Prato("Huramaki + Hosomaki mix", "Seleção de huramaki e hosomaki, caixa média com 24 peças", (float) 8.5, 500,
						new ArrayList<Opcao>(Arrays.asList(
							new Opcao("Caixa grande, com 50 peças", (float) 11.2, 600),
							new Opcao("Wasabi", (float) 0.3, 5),
							new Opcao("Soja", (float) 0.3, 5)
						))
					)
				))
			)
		);

		// Restaurante: Churrasqueira da EST
		come.addRestaurante(
			new Restaurante("Churrasqueira da EST", "Churrascos na brasa (de carvão) como nunca comeu.",
				new ArrayList<Prato>(Arrays.asList(
					new Prato("Frango Assado", "Frango assado inteiro (1 unidade)", (float) 8.5, 800,
						new ArrayList<Opcao>(Arrays.asList(
							new Opcao("Só 1/2 Frango", (float) -2.5, -350),
							new Opcao("Molho especial", (float) 0, 10),
							new Opcao("Molho picante", (float) 0, 10), new Opcao("Arroz", (float) 1.3, 200),
							new Opcao("Batata frita", (float) 1.3, 200)
						))
					),
					new Prato("Picanha", "Picanha do Brasil (dose de 600g)", (float) 13.5, 600,
						new ArrayList<Opcao>(Arrays.asList(
							new Opcao("Dose de 1kg", (float) 3.5, 400),
							new Opcao("Arroz", (float) 1.3, 200),
							new Opcao("Feijão preto", (float) 2.2, 300)
						))
					),
					new Prato("Secretos", "Secretos de porto preto (dose de 500g)", (float) 10.5, 500,
						new ArrayList<Opcao>(Arrays.asList(
							new Opcao("Arroz", (float) 1.3, 200),
							new Opcao("Feijão preto", (float) 2.2, 300)
						))
					)
				))
			)
		);

		// Criar o menu principal do servidor
		Thread t1 = new Thread() {
			public void run() {
				MenuServidor app = new MenuServidor(come);
				app.menuPrincipal();
			};
		};
		t1.start();

		// Criar o menu da aplicação móvel
		Thread t2 = new Thread() {
			public void run() {
				MenuPedidos appMovel = new MenuPedidos(come);
				appMovel.menuPrincipal();
			};
		};
		t2.start();
	}
}
