package menus;

import java.util.List;

import consola.SConsola;
import restaurante.*;
import sistema.*;

/**
 * Classe que trata dos menus que simula a "aplicação movel"
 */
public class MenuPedidos {

	private SConsola consola = new SConsola("Menu ComEST", 630, 30, 500, 600);
	private ComEST server; // servidor ao qual o simulador está ligado

	/**
	 * Cria o menu da aplicação móvel
	 * 
	 * @param s o servidor a que vai ligar
	 * @param u o utilizador a simular inicialmente
	 */
	public MenuPedidos(ComEST s) {
		server = s;
	}

	/**
	 * Menu que simula o telemóvel da aplicação
	 */
	public void menuPrincipal() {

		char opcao = 0;
		do {
			String menu = "Menu de consumidor\n" + "1- Fazer pedido\n" + "2- Ver pedido\n";

			consola.clear();
			consola.println(menu);
			opcao = consola.readChar();
			switch (opcao) {
			case '1':
				fazerPedido();
				break;
			case '2':
				verPedido();
				break;
			default:
				consola.println("opção inválida");
				consola.readLine();
			}
		} while (true);
	}

	/**
	 * Regista um código na aplicação
	 */
	private void fazerPedido() {
		consola.clear();
		Restaurante r = escolherRestaurante();
		if (r == null)
			return;
		Pedido p = server.genPedido(r);

		int pidx = 0;
		do {
			consola.clear();
			consola.println(r.getNome() + "\n\nPratos já pedidos");

			printEscolhas(p.getEscolhas());

			consola.println(String.format("\nCusto pedido:  %6.2f€", p.getPreco()));
			consola.println(String.format("Custo entrega: %6.2f€  por  %4dg", p.getTarifa(), p.getPeso()));
			consola.println(String.format("Total:         %6.2f€\n", p.getPrecoTotal()));
			consola.println(
					"Escolha um prato\n0 para confirmar o pedido\n-1 para cancelar\n\n" + "Pratos disponibilizados");

			printPratos(r.getPratos());
			consola.print("\nPrato: ");
			pidx = consola.readInt();
			if (pidx > 0 && pidx <= r.getPratos().size()) {
				escolherOpcoesPrato(p, r.getPratos().get(pidx - 1));
			}
		} while (pidx != 0 && pidx != -1);

		consola.clear();
		if (pidx == 0) {
			if (!p.estaVazio()) {
				consola.println(
						"O seu pedido foi aceite!\n\nPara saber informações use o código " + server.addPedido(p));
			} else {
				consola.println("O seu pedido não for confirmado porque não tinha nenhum prato escolhido!");
			}
		} else {
			consola.println("O seu pedido foi cancelado a seu pedido!");
		}
		consola.readLine();
	}

	/**
	 * Apresenta, na consola, uma lista de escolhas
	 * 
	 * @param escolhas a lista de escolhas a apresentar
	 */
	private void printEscolhas(List<Escolha> escolhas) {
		if (escolhas.size() == 0) {
			consola.println("<Ainda sem pratos no pedido>");
			return;
		}

		for (Escolha e : escolhas) {
			Prato p = e.getPrato();
			consola.println(String.format("%-40s %6.2f€", p.getNome(), p.getPreco()));
			for (Opcao o : e.getOpcoes()) {
				consola.println(String.format("     %-35s %6.2f€", o.getNome(), o.getCusto()));
			}
		}
	}

	/**
	 * Apresenta, na consola, uma lista de pratos
	 * 
	 * @param pratos a lista de pratos a apresentar
	 */
	private void printPratos(List<Prato> pratos) {
		if (pratos.size() == 0) {
			consola.println("<Sem pratos>");
			return;
		}

		for (int i = 0; i < pratos.size(); i++) {
			Prato p = pratos.get(i);
			consola.println(String.format("%2d: %-40s %6.2f€", i + 1, p.getNome(), p.getPreco()));
		}
	}

	/**
	 * Apresenta os restaurantes associados e pede ao utilizador para escolher um
	 * 
	 * @return o restaurante selecionado ou null, caso não haja seleção
	 */
	private Restaurante escolherRestaurante() {
		for (int i = 0; i < server.getRestaurantes().size(); i++) {
			consola.println((i + 1) + ": " + server.getRestaurante(i).getNome());
		}

		consola.print("\nRestaurante: ");
		Restaurante r = server.getRestaurante(consola.readInt() - 1);
		if (r == null) {
			consola.println("Restaurante inválido!");
			consola.readLine();
			return null;
		}

		consola.println("\n\n" + r.getNome() + "\n" + r.getDesc() + "\n\nOferta:");
		printPratos(r.getPratos());

		consola.println("Escolher o restaurante? (0: confirmar)");
		int sim = consola.readInt();
		return sim == 0 ? r : null;
	}

	/**
	 * Apresenta as opções do prato selecionado e pede para confirmar o prato como
	 * sendo escolhido
	 * 
	 * @param pedido o pedido em processamento
	 * @param prato  o prato a ser observado e eventualemente escolhido
	 */
	private void escolherOpcoesPrato(Pedido pedido, Prato prato) {
		int numOpcoes = prato.getOpcoes().size();
		boolean select[] = new boolean[numOpcoes];
		Escolha e = pedido.genEscolha(prato);

		String infoPrato = prato.getNome() + "\n" + prato.getDesc() + "\nPreço base: "
				+ String.format("%6.2f€", prato.getPreco()) + "\n\n"
				+ "Selecione (desselecione) as opções pretendidas\n" + "0 para confirmar\n-1 para cancelar\n";

		int oidx = 0;
		do {
			consola.clear();
			consola.println(infoPrato);
			consola.println(String.format("Custo: %6.2f €", pedido.getPrecoTotal()));
			for (int i = 0; i < numOpcoes; i++) {
				Opcao o = prato.getOpcoes().get(i);
				consola.print(select[i] ? "(o) " : "( ) ");
				consola.println(String.format("%2d: %-35s %6.2f€", i + 1, o.getNome(), o.getCusto()));
			}
			consola.println("Opção: ");
			oidx = consola.readInt();
			if (oidx > 0 && oidx <= numOpcoes) {
				int idx = oidx - 1;
				select[idx] = !select[idx];
				if (select[idx])
					e.addOpcao(idx);
				else
					e.delOpcao(idx);
			}
		} while (oidx != 0 && oidx != -1);
		if (oidx == 0) {
			pedido.addEscolha(e);
		}
	}

	/**
	 * Apresenta informação sucinta sobre um dado pedido
	 */
	private void verPedido() {
		consola.clear();
		consola.println("Código? ");
		Pedido p = server.getPedido(consola.readLine());
		if (p == null) {
			consola.println("Pedido inexistente!");
			consola.readLine();
			return;
		}

		consola.clear();
		consola.println("Pedido " + p.getCodigo() + "\nRestaurante: " + p.getRestaurante().getNome() + "\nPreço: "
				+ p.getPrecoTotal() + "€\n\n");

		for (Escolha e : p.getEscolhas()) {
			consola.println(e.getPrato().getNome());
			for (int i = 0; i < e.getOpcoes().size(); i++) {
				Opcao o = e.getOpcoes().get(i);
				consola.println("    " + o.getNome());
			}
		}
		consola.readLine();
	}
}
