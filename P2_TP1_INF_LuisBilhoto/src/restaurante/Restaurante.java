package restaurante;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sistema.Pedido;

/**
 * Representa um restaurante Cada restaurante deve ter um nome e uma descrição.
 * A descrição deve ser uma apresentação do restaurante indicando o seu tipo e
 * filosofia. Deve ainda ter uma lista com os pratos que disponibiliza. O
 * restaurante deve ainda ter uma lista dos pedidos que os clientes já
 * colocaram.
 */
public class Restaurante {
	private String nome;
	private String desc;

	/* Lista de pratos e encomendas efetuados */
	private List<Prato> pratos = new ArrayList<Prato>();
	private Map<String, Pedido> encomendas = new HashMap<String, Pedido>();

	public Restaurante(String nome, String desc, List<Prato> pratos) {
		setNome(nome);
		setDesc(desc);
		this.pratos = pratos;
	}

	public Restaurante(String nome, String desc) {
		setNome(nome);
		setDesc(desc);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public List<Prato> getPratos() {
		return Collections.unmodifiableList(pratos);
	}

	public void addPrato(Prato p) {
		pratos.add(p);
	}

	public void delPrato(Prato p) {
		pratos.remove(p);
	}

	public void delPrato(int i) {
		pratos.remove(i);
	}

	public Collection<Pedido> getEncomendas() {
		return Collections.unmodifiableCollection(encomendas.values());
	}

	public void addEncomenda(Pedido p) {
		encomendas.put(p.getCodigo(), p);
	}

	public void delEncomenda(Pedido p) {
		encomendas.remove(p.getCodigo());
	}

	public void delEncomenda(String s) {
		encomendas.remove(s);
	}

	/**
	 * Indica se um dado prato pertence a este restaurante
	 * 
	 * @param p o prato a verificar
	 * @return true, se o prato faz parte da lista do restaurante
	 */
	public boolean temPrato(Prato p) {
		return pratos.contains(p);
	}

	@Override
	public String toString() {
		return String.format("Restaurante [nome=%s, desc=%s, pratos=%s, encomendas=%s]", nome, desc, pratos,
				encomendas);
	}
}
