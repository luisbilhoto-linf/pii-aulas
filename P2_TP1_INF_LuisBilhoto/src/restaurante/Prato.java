package restaurante;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe que representa um prato do sistema. O prato deve ter um nome que o
 * identifica. Deve ter ainda uma descrição, que é uma explicação do que o prato
 * contém ou o que é. Cada prato deve ter um preço (sempre positivo) e um peso
 * (sempre positivo). Um prato pode ainda ter uma lista de opções.
 */
public class Prato {
	private String nome;
	private String desc;
	private float preco;
	private int peso;

	/* Lista de opções */
	private List<Opcao> opcoes = new ArrayList<Opcao>();

	public Prato(String nome, String desc, float preco, int peso, List<Opcao> opcoes) {
		setNome(nome);
		setDesc(desc);
		setPreco(preco);
		setPeso(peso);
		this.opcoes = opcoes;
	}

	public Prato(String nome, String desc, float preco, int peso) {
		setNome(nome);
		setDesc(desc);
		setPreco(preco);
		setPeso(peso);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public float getPreco() {
		return preco;
	}

	public void setPreco(float preco) {
		this.preco = preco >= 0 ? preco : 0;
	}

	public int getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = peso >= 0 ? peso : 0;
	}

	public List<Opcao> getOpcoes() {
		return Collections.unmodifiableList(opcoes);
	}

	public void addOpcao(Opcao o) {
		opcoes.add(o);
	}

	public void delOpcao(Opcao o) {
		opcoes.remove(o);
	}

	public void delOpcao(int i) {
		opcoes.remove(i);
	}

	/**
	 * Indica se a opção indicada é válida para este prato
	 * 
	 * @param o a opção a verificar
	 * @return true, se a opção é válida para o prato.
	 */
	public boolean temOpcao(Opcao o) {
		return opcoes.contains(o);
	}

	@Override
	public String toString() {
		return String.format("Prato [nome=%s, desc=%s, preco=%s, peso=%s, opcoes=%s]", nome, desc, preco, peso, opcoes);
	}
}
