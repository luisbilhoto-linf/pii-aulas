package restaurante;

/**
 * Classe que representa uma opção de um prato. Uma opção deve ter um nome, que
 * indica o que ela é. Deve ainda ter um custo (que pode ser negativo) e um peso
 * (que pode ser negativo).
 */
public class Opcao {
	private String nome;
	private float custo;
	private int peso;

	public Opcao(String nome, float custo, int peso) {
		setNome(nome);
		setCusto(custo);
		setPeso(peso);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public float getCusto() {
		return custo;
	}

	public void setCusto(float custo) {
		this.custo = custo;
	}

	public int getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}

	@Override
	public String toString() {
		return String.format("Opcao [nome=%s, custo=%s, peso=%s]", nome, custo, peso);
	}
}
