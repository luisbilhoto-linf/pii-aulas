package sistema;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import restaurante.Opcao;
import restaurante.Prato;

/**
 * Representa um prato escolhido pelo cliente e respetivas opções selecionadas
 * Deve ter a indicação de qual o prato escolhido e uma lista com as opções
 * selecionadas. Ao adicionar uma opção dever verificar se a mesma faz parte das
 * opções suportadas pelo prato.
 */
public class Escolha {
	private Prato prato;

	/* Lista de opções escolhidas */
	private List<Opcao> opcoes = new ArrayList<Opcao>();

	public Escolha(Prato prato) {
		setPrato(prato);
	}

	public Prato getPrato() {
		return prato;
	}

	private void setPrato(Prato prato) {
		this.prato = prato;
	}

	public List<Opcao> getOpcoes() {
		return Collections.unmodifiableList(opcoes);
	}

	public void addOpcao(Opcao o) {
		if (getPrato().getOpcoes().contains(o)) {
			opcoes.add(o);
		}
	}

	/**
	 * Adiciona a opção à escolha baseando-se na posição da opção do prato.
	 * 
	 * @param i posição da opção do prato
	 */
	public void addOpcao(int i) {
		addOpcao(prato.getOpcoes().get(i));
	}

	public void delOpcao(Opcao o) {
		opcoes.remove(o);
	}

	/**
	 * Remove a opção à escolha baseando-se na posição da opção do prato.
	 * 
	 * @param i posição da opção do prato
	 */
	public void delOpcao(int i) {
		delOpcao(prato.getOpcoes().get(i));
	}

	/**
	 * Retorna o peso total da escolha, ou seja, o peso do prato mais o peso de cada
	 * uma das opções selecionadas.
	 * 
	 * @return o peso total da escolha
	 */
	public int getPeso() {
		int peso = prato.getPeso();
		for (Opcao op : opcoes) {
			peso += op.getPeso();
		}
		return peso;
	}

	/**
	 * Retorna o preço total da escolha, ou seja, o preço do prato mais o preço de
	 * cada uma das opções selecionadas.
	 * 
	 * @return o preço total da escolha
	 */
	public float getPreco() {
		float preco = prato.getPreco();
		for (Opcao op : opcoes) {
			preco += op.getCusto();
		}
		return preco;
	}

	@Override
	public String toString() {
		return String.format("Escolha [prato=%s, opcoes=%s]", prato, opcoes);
	}
}
