package sistema;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import restaurante.Prato;
import restaurante.Restaurante;

/**
 * Representa um pedido de comida. Um pedido deve indicar qual o restaurante
 * selecionado e ter uma lista de escolhas. Quando se adiciona uma escolha ao
 * pedido este deve verificar se o prato é do restaurante ao qual o pedido está
 * associado.
 */
public class Pedido {
	Restaurante restaurante;
	String codigo;

	/* Lista de escolhas selecionadas */
	List<Escolha> escolhas = new ArrayList<Escolha>();

	public Pedido(Restaurante restaurante) {
		setRestaurante(restaurante);
	}

	public Restaurante getRestaurante() {
		return restaurante;
	}

	private void setRestaurante(Restaurante restaurante) {
		this.restaurante = restaurante;
	}

	public String getCodigo() {
		return codigo;
	}

	protected void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public List<Escolha> getEscolhas() {
		return Collections.unmodifiableList(escolhas);
	}

	public boolean addEscolha(Escolha e) {
		if (getRestaurante().getPratos().contains(e.getPrato())) {
			escolhas.add(e);
			return true;
		}
		return false;
	}

	public Escolha genEscolha(Prato p) {
		return new Escolha(p);
	}

	public void delEscolha(Escolha e) {
		escolhas.remove(e);
	}

	public void delEscolha(int i) {
		escolhas.remove(i);
	}

	/**
	 * Retorna o peso total do pedido, ou seja, com todas as escolhas selecionadas.
	 * 
	 * @return o peso total do pedido
	 */
	public int getPeso() {
		int peso = 0;
		for (Escolha e : escolhas) {
			peso += e.getPeso();
		}
		return peso;
	}

	/**
	 * Retorna o preço do pedido, ou seja, com todas as escolhas selecionadas.
	 * 
	 * @return o preço do pedido (sem tarifa)
	 */
	public float getPreco() {
		float preco = 0;
		for (Escolha e : escolhas) {
			preco += e.getPreco();
		}
		return preco;
	}

	public float getTarifa() {
		int peso = getPeso();
		if (peso < 0)
			return 0f;
		if (peso <= 1500)
			return 2.5f;
		if (peso <= 3000)
			return 4.5f;
		if (peso <= 4000)
			return 5.0f;
		return 6.0f + (1 * ((peso - 4000) / 1000));
	}

	/**
	 * Retorna o preço total do pedido, ou seja, com todas as escolhas selecionadas.
	 * 
	 * @return o preço do pedido (com tarifa)
	 */
	public float getPrecoTotal() {
		return getPreco() + getTarifa();
	}

	/**
	 * Indica se o pedido não tem escolhas associadas
	 * 
	 * @return true, se o pedido não tiver qualquer escolha
	 */
	public boolean estaVazio() {
		return escolhas.isEmpty();
	}

	@Override
	public int hashCode() {
		return Objects.hash(codigo);
	}

	@Override
	public boolean equals(Object obj) {
		return this == obj && obj != null && getClass() == obj.getClass() && codigo == ((Pedido) obj).codigo;
	}

	@Override
	public String toString() {
		return String.format("Pedido [restaurante=%s, codigo=%s, escolhas=%s]", restaurante, codigo, escolhas);
	}
}
