package sistema;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import restaurante.Restaurante;

/**
 * Classe que representa todo o sistema Deve ter uma lista de restaurantes
 * suportados e outra dos pedidos efetuados Deve ser a responsável por atribuir
 * o código a um pedido, quando este é adicionado ao sistema
 */
public class ComEST {
	private List<Restaurante> restaurantes = new ArrayList<Restaurante>();
	private Map<String, Pedido> pedidos = new HashMap<String, Pedido>();

	public List<Restaurante> getRestaurantes() {
		return Collections.unmodifiableList(restaurantes);
	}

	public Restaurante getRestaurante(int i) {
		return i >= 0 && i < restaurantes.size() ? restaurantes.get(i) : null;
	}

	public void addRestaurante(Restaurante r) {
		restaurantes.add(r);
	}

	public void delRestaurante(Restaurante r) {
//		DISABLED: Remoção de pedidos pertencentes ao restaurante a ser removido
//		for (Pedido e : r.getEncomendas()) {
//			pedidos.remove(e.getCodigo());
//		}
		restaurantes.remove(r);
	}

	public void delRestaurante(int i) {
		delRestaurante(getRestaurante(i));
	}

	public Collection<Pedido> getPedidos() {
		return Collections.unmodifiableCollection(pedidos.values());
	}

	public Pedido getPedido(String s) {
		return pedidos.get(s);
	}

	public String addPedido(Pedido p) {
		p.setCodigo(GeradorCodigos.gerarCodigo(6));
		p.getRestaurante().addEncomenda(p);
		pedidos.put(p.getCodigo(), p);
		return p.getCodigo();
	}

	public Pedido genPedido(Restaurante r) {
		return new Pedido(r);
	}

	public void delPedido(Pedido p) {
//		DISABLED: Remoção da encomenda pertencente ao pedido a ser removido
//		p.getRestaurante().delEncomenda(p.getCodigo());
		pedidos.remove(p.getCodigo());
	}

	public void delPedido(String s) {
		delPedido(getPedido(s));
	}
}
