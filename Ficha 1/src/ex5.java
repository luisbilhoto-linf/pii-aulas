import java.util.Scanner;

public class ex5 {

	public static int daysInYear(int year) {
		return ex4.isLeapYear(year) ? 366 : 365;
	}

	public static void main(String[] args) {
		System.out.println("N�MERO DE DIAS DE UM ANO");
		System.out.println("========================");
		Scanner input = new Scanner(System.in);

		while (true) {
			System.out.print("Introduza um ano: ");
			System.out.printf("O ano introduzido tem %d dias.\n\n", daysInYear(input.nextInt()));
		}
	}
}
