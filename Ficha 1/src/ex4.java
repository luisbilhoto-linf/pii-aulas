import java.util.Scanner;

public class ex4 {

	public static boolean isLeapYear(int year) {
		return year % 400 == 0 || (year % 4 == 0 && year % 100 != 0);
	}

	public static void main(String[] args) {
		System.out.println("ANOS BISSEXTOS E COMUNS");
		System.out.println("=======================");
		Scanner input = new Scanner(System.in);

		while (true) {
			System.out.print("Introduza um ano: ");
			System.out.printf("O ano introduzido � %s.\n\n", isLeapYear(input.nextInt()) ? "bissexto" : "comum");
		}
	}
}
