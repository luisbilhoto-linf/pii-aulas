import java.util.Scanner;

public class ex9n10 {
	private static int npessoas = 0;
	private static int maxArray = 3000;
	private static String names[] = new String[maxArray];
	private static String address[] = new String[maxArray];
	private static String postal[] = new String[maxArray];
	private static String local[] = new String[maxArray];
	private static String phone[] = new String[maxArray];
	private static String birth[] = new String[maxArray];
	private static char marital[] = new char[maxArray];

	/**
	 * inputChar
	 * 
	 * Este m�todo retorna um caractere. Caso na linha n�o seja possivel encontrar
	 * nenhum caracter, este retorna (char)13, ou seja o caracter da tecla 'ENTER'.
	 * 
	 * @param input Scanner declarado em main;
	 */
	private static char inputChar(Scanner input) {
		return (input.nextLine() + (char) 13).charAt(0);
	}

	private static void printPessoa(int i) {
		System.out.printf("\nNome: %s\n", names[i]);
		System.out.printf("Morada: %s\n", address[i]);
		System.out.printf("Telefone: %s\n", phone[i]);
		System.out.printf("Data de nascimento: %s\n", birth[i]);
		System.out.print("Estado civil: ");
		switch (marital[i]) {
		case 's':
			System.out.print("solteiro");
			break;
		case 'c':
			System.out.print("casado");
			break;
		case 'd':
			System.out.print("divorciado");
			break;
		case 'v':
			System.out.print("vi�vo");
			break;
		case 'u':
			System.out.print("uni�o de facto");
			break;
		default:
			System.out.print("desconhecido");
			break;
		}
		System.out.print("\n");
	}

	private static void printPessoas() {
		for (int i = 0; i < npessoas; i++) {
			printPessoa(i);
		}
	}

	private static void printPessoasMaioresIdade(int aRef) {
		for (int i = 0; i < npessoas; i++) {
			if (eMaiorIdade(getAnoNasc(i), aRef)) {
				printPessoa(i);
			}
		}
	}

	private static int getAnoNasc(int i) {
		return Integer.parseInt(birth[i].split("/")[2]);
	}

	private static int getIdade(int anoNasc, int anoReferencia) {
		return anoReferencia - anoNasc;
	}

	private static boolean eMaiorIdade(int anoNasc, int anoReferencia) {
		return getIdade(anoNasc, anoReferencia) >= 18;
	}

	private static int getIdade(int d, int m, int a, int dRef, int mRef, int aRef) {
		int idade = getIdade(a, aRef);
		if (mRef < m || dRef < d) {
			idade--;
		}
		return idade;
	}

	/**
	 * printWait
	 * 
	 * Este m�todo imprime uma mensagem de espera e sucessivamente espera pelo input
	 * de qualquer coisa. Funcionalidade de embelezamento e melhorias de
	 * usabilidade.
	 * 
	 * @param input Scanner declarado em main;
	 */
	private static void printWait(Scanner input) {
		System.out.println("\nPressione a tecla ENTER para continuar...");
		input.nextLine(); // Wait until user press ENTER key.
	}

	/**
	 * menuDefault
	 * 
	 * Apresenta a mensagem de op��o n�o reconhecida. Espera at� o utilizador
	 * pressionar a tecla ENTER para terminar.
	 */
	public static void menuDefault(Scanner input, char menu) {
		System.out.printf("Op��o '%s' n�o reconhecida! Tente novamente.\n", menu == 13 ? "ENTER" : menu);
		printWait(input);
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		char menu = 'S';

		do {
			System.out.println("MENU PRINCIPAL");
			System.out.println("A - Adicionar Pessoa");
			System.out.println("B - Mostrar Pessoa");
			System.out.println("C - Mostrar Pessoas");
			System.out.println("S - Sair");
			menu = Character.toUpperCase(inputChar(input));
			int[] temp;
			switch (menu) {
			case 'A':
				System.out.print("\nNome: ");
				names[npessoas] = input.nextLine().trim();
				System.out.print("Morada: ");
				address[npessoas] = input.nextLine().trim();
				System.out.print("C�digo postal: ");
				postal[npessoas] = input.nextLine().trim();
				System.out.print("Localidade: ");
				local[npessoas] = input.nextLine().trim();
				System.out.print("Telefone: ");
				phone[npessoas] = input.nextLine().trim();
				System.out.print("Data de nascimento (d/m/a): ");
				birth[npessoas] = input.nextLine().trim();
				System.out.print("Estado civil (s|c|d|v|u): ");
				marital[npessoas] = input.nextLine().charAt(0);
				npessoas++;
				System.out.println("A pessoa foi adiciona com sucesso!");
				printWait(input);
				break;
			case 'B':
				System.out.print("\nPosi��o: ");
				printPessoa(input.nextInt());
				input.nextLine(); // System.in junk reset
				printWait(input);
				break;
			case 'C':
				printPessoas();
				printWait(input);
				break;
			case 'S':
				break;

			default:
				menuDefault(input, menu);
				break;
			}
		} while (menu != 'S');
		input.close();
	}
}
