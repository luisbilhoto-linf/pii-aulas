import java.util.Scanner;

public class ex7n8 {

	public static int compareDates(String date1, String date2) {
		int y1 = Integer.parseInt(date1.split("/")[2]);
		int y2 = Integer.parseInt(date2.split("/")[2]);
		int d = ex6.daysSpentInYear(date2);
		if (y1 < y2) {
			d -= ex5.daysInYear(y1) - ex6.daysSpentInYear(date1);
			for (int i = y1 + 1; i < y2; i++) {
				d -= ex5.daysInYear(i);
			}
			return d;
		}
		if (y1 > y2) {
			d += ex5.daysInYear(y1) + ex6.daysSpentInYear(date1);
			for (int i = y1 - 1; i > y2; i--) {
				d += ex5.daysInYear(i);
			}
			return d;
		}
		return ex6.daysSpentInYear(date1) - d;
	}

	public static void main(String[] args) {
		System.out.println("DIAS DE DIFEREN�A ENTRE DUAS DATAS");
		System.out.println("==================================");
		Scanner input = new Scanner(System.in);

		String inputString = "00/00/0000";
		while (true) {
			System.out.print("Introduza a 1� data (d/m/a): ");
			inputString = input.nextLine().trim();
			System.out.print("Introduza a 2� data (d/m/a): ");
			System.out.printf("As datas introduzida tem %d dias de diferen�a.\n\n",
					compareDates(inputString, input.nextLine().trim()));
		}
	}
}
