import java.util.Scanner;

public class ex2 {

	public static int hoursToSeconds(String string) {
		String hour[] = string.split(":");
		int h = Integer.parseInt(hour[0]);
		int m = Integer.parseInt(hour[1]);
		int s = Integer.parseInt(hour[2]);
		return (h * 3600) + (m * 60) + s;
	}

	public static void main(String[] args) {
		System.out.println("SEGUNDOS DE UMA HORA");
		System.out.println("====================");
		Scanner input = new Scanner(System.in);

		while (true) {
			System.out.print("Introduza uma hora (hh:mm:ss): ");
			System.out.printf("A hora inserida corresponde a %d segundos.\n\n",
					hoursToSeconds(input.nextLine().trim()));
		}
	}
}
