import java.util.Scanner;

public class ex6 {

	public static int daysSpentInYear(String string) {
		String date[] = string.split("/");
		int d = Integer.parseInt(date[0]);
		int m = Integer.parseInt(date[1]);
		int y = Integer.parseInt(date[2]);
		int dim[] = { 31, ex4.isLeapYear(y) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		for (int i = 0; i < (m - 1); i++) {
			d += dim[i];
		}
		return d;
	}

	public static void main(String[] args) {
		System.out.println("DIAS PASSADOS DESDE O IN�CIO DO ANO");
		System.out.println("===================================");
		Scanner input = new Scanner(System.in);

		while (true) {
			System.out.print("Introduza uma data (d/m/a): ");
			System.out.printf("J� se passaram %d dias no ano.\n\n", daysSpentInYear(input.nextLine().trim()));
		}
	}
}
