import java.util.Scanner;

public class ex1 {

	public static int numModulus(int num) {
		return num < 0 ? -(num) : num;
	}

	public static void main(String[] args) {
		System.out.println("M�DULO DE UM N�MERO INTEIRO");
		System.out.println("===========================");
		Scanner input = new Scanner(System.in);

		while (true) {
			System.out.print("Introduza um n�mero inteiro: ");
			System.out.printf("O valor do m�dulo do n�mero inserido �: %d\n\n", numModulus(input.nextInt()));
		}
	}
}
