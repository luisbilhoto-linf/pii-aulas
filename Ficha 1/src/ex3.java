import java.util.Scanner;

public class ex3 {

	public static int compareHours(String hour1, String hour2) {
		return ex1.numModulus(ex2.hoursToSeconds(hour1) - ex2.hoursToSeconds(hour2));
	}

	public static void main(String[] args) {
		System.out.println("DIFEREN�A ENTRE DUAS HORAS");
		System.out.println("==========================");
		Scanner input = new Scanner(System.in);

		String inputString = "00:00:00";
		while (true) {
			System.out.print("Introduza a 1� hora (hh:mm:ss): ");
			inputString = input.nextLine().trim();
			System.out.print("Introduza a 2� hora (hh:mm:ss): ");
			System.out.printf("A diferen�a entre as duas horas � de %d segundos.\n\n",
					compareHours(inputString, input.nextLine().trim()));
		}
	}
}
