package p2.freecell;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

import p2.carta.Carta;

public abstract class ZonaDefault implements Zona {
	private ArrayList<Carta> asCartas = new ArrayList<Carta>();
	private Point topo;
	private int comprimento;
	private int altura;
	private boolean selecionado = false;

	/**
	 * Cria a zona numa determinada posição do écran, definindo o seu comprimento e
	 * largura
	 * 
	 * @param t    coordenada do topo da zona
	 * @param comp comprimento da zona
	 * @param alt  altura da zona
	 */
	public ZonaDefault(Point t, int comp, int alt) {
		topo = t;
		comprimento = comp;
		altura = alt;
	}

	/**
	 * Indica se pode receber a carta.
	 * 
	 * @param c a carta a verificar
	 * @return true se pode receber, false em caso contrário
	 */
	public abstract boolean podeReceber(Carta c);

	/**
	 * Recebe a carta, se obedecer às regras
	 * 
	 * @param c a carta a receber
	 * @return true se recebeu, false caso contrário
	 */
	public boolean receber(Carta c) {
		if (!podeReceber(c))
			return false;
		colocar(c);
		return true;
	}

	/**
	 * Coloca a carta mesmo que não obedeça às regras
	 * 
	 * @param c carta a colocar
	 */
	public void colocar(Carta c) {
		asCartas.add(c);
	}

	/**
	 * Retira a carta e devolve-a
	 * 
	 * @return a carta retirada
	 */
	public Carta retirar() {
		return (Carta) asCartas.remove(asCartas.size() - 1);
	}

	/**
	 * Devolve a carta no topo da coluna, sem a retirar
	 * 
	 * @return a carta ou null se vazia
	 */
	public Carta getCarta() {
		return (Carta) asCartas.get(asCartas.size() - 1);
	}

	/**
	 * Devolve a posição no écran
	 * 
	 * @return
	 */
	public Point getPosicao() {
		return topo;
	}

	/**
	 * Indica se está vazia, isto é, não tem cartas
	 * 
	 * @return se está vazia
	 */
	public boolean estaVazio() {
		return asCartas.isEmpty();
	}

	/**
	 * Devolve o comprimento
	 * 
	 * @return o comprimento em pixeis
	 */
	public int getComprimento() {
		return comprimento;
	}

	/**
	 * Devolve a altura em pixeis
	 * 
	 * @return a altura em pixeis
	 */
	public int getAltura() {
		return altura;
	}

	/**
	 * Define uma nova posição no écran para a coluna
	 * 
	 * @param p a nova posição no écran
	 */
	public void setPosicao(Point p) {
		int dx = p.x - topo.x;
		int dy = p.y - topo.y;
		topo = p;

		// reposicionar as cartas
		for (int i = 0; i < asCartas.size(); i++) {
			Point op = asCartas.get(i).getPosicao();
			asCartas.get(i).setPosicao(new Point(op.x + dx, op.y + dy));
		}
	}

	/**
	 * Remove todas as cartas
	 */
	public void limpar() {
		asCartas.clear();
	}

	/**
	 * Indica se uma dada coordenada está dentro do espaço da coluna
	 * 
	 * @param pt a coordenada a verificar
	 * @return true se a coordenada está dentro, false caso contrário
	 */
	public boolean estaDentro(Point pt) {
		// ver se clicou em alguma das cartas do componente
		for (int i = 0; i < asCartas.size(); i++)
			if (((Carta) asCartas.get(i)).estaDentro(pt))
				return true;

		// senão ver se clicou na área do componente
		return topo.x <= pt.x && topo.y <= pt.y && topo.x + comprimento >= pt.x && topo.y + altura >= pt.y;
	}

	/**
	 * (Des)Selecciona a coluna
	 * 
	 * @param sel true para seleccionar, false para desseleccionar
	 */
	public void setSeleccionado(boolean sel) {
		selecionado = sel;
		if (!estaVazio())
			getCarta().setSelecionada(sel);
	}

	/**
	 * Desenha a coluna no écran
	 * 
	 * @param g sistema gráfico onde se vai desenhar
	 */
	public void desenhar(Graphics g) {
		for (int i = 0; i < asCartas.size(); i++) {
			Carta card = (Carta) asCartas.get(i);
			card.desenhar(g);
		}
	}
}
