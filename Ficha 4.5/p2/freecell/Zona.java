package p2.freecell;

import java.awt.Graphics;
import java.awt.Point;

import p2.carta.Carta;

public interface Zona {
	public abstract boolean podeReceber(Carta c);

	public boolean receber(Carta c);

	public void colocar(Carta c);

	public Carta retirar();

	public Carta getCarta();

	public Point getPosicao();

	public boolean estaVazio();

	public int getComprimento();

	public int getAltura();

	public void setPosicao(Point p);

	public void limpar();

	public boolean estaDentro(Point pt);

	public void setSeleccionado(boolean sel);

	public void desenhar(Graphics g);
}
