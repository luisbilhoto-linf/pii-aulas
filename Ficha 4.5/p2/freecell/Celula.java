package p2.freecell;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import p2.carta.Carta;

/**
 * Representa uma célula, local onde se pode armazenar uma e uma só carta
 */
public class Celula extends ZonaDefault {

	/**
	 * Cria a célula numa determinada posição do écran, definindo o seu comprimento
	 * e largura
	 * 
	 * @param t    coordenada do topo da célula
	 * @param comp comprimento da célula
	 * @param alt  altura da célula
	 */
	public Celula(Point t, int comp, int alt) {
		super(t, comp, alt);
	}

	/**
	 * Indica se pode receber a carta
	 * 
	 * @param c a carta a verificr
	 * @return true se pode receber, false em caso contrário
	 */
	public boolean podeReceber(Carta c) {
		return estaVazio();
	}

	/**
	 * Coloca a carta mesmo que não obedeça às regras
	 * 
	 * @param c carta a colocar
	 */
	public void colocar(Carta c) {
		c.setPosicao(new Point(getPosicao().x + 1, getPosicao().y + 1));
		super.colocar(c);
	}

	/**
	 * Desenha a célula no écran
	 * 
	 * @param g sistema gráfico onde se vai desenhar
	 */
	public void desenhar(Graphics g) {
		int x1 = getPosicao().x;
		int y1 = getPosicao().y;
		int x2 = getPosicao().x + getComprimento();
		int y2 = getPosicao().y + getAltura();

		g.setColor(Color.black);
		g.drawLine(x1, y1, x2, y1);
		g.drawLine(x1, y1, x1, y2);
		g.setColor(Color.green);
		g.drawLine(x2, y1 + 1, x2, y2);
		g.drawLine(x1 + 1, y2, x2, y2);

		super.desenhar(g);
	}
}
