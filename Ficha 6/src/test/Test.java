package test;

import banco.Conta;
import banco.SaldoInsuficienteException;

public class Test {

	public static void main(String[] args) {
		Conta conta1 = new Conta(0, 999);
		
		try {
			conta1.levantar(1000);
		} catch (SaldoInsuficienteException e) {
			e.printStackTrace();
		}
		
		System.out.println(conta1.toString());
	}
}
