package cartao;

import banco.Conta;
import banco.SaldoInsuficienteException;

public class CartaoFimMes extends CartaoDefault {
	private long compras, levantamentos;

	public CartaoFimMes(int id, Conta conta, long plafond) {
		super(id, conta, plafond);
	}

	@Override
	public void compra(long quantia) throws SaldoInsuficienteException, CartaoInativoException {
		checkAtivo();
		gastaPlafond(quantia);
		compras += quantia;
	}

	@Override
	public void levanta(long quantia) throws CartaoInativoException, SaldoInsuficienteException {
		checkAtivo();

		try {
			getConta().levantar(quantia);
		} catch (SaldoInsuficienteException e) {
			long pagar = quantia - getConta().getSaldo();
			gastaPlafond(pagar);
			getConta().levantar(getConta().getSaldo());
			levantamentos += pagar;
		}
	}

	@Override
	public void resetPlafond() throws CartaoInativoException {
		checkAtivo();
		long total = (long) (compras * 1.02 + levantamentos * 1.1);
		setDivida(0);
		try {
			levanta(total);
		} catch (SaldoInsuficienteException e) {
			setActivo(false);
		}
	}
}
