package cartao;

import banco.Conta;
import banco.SaldoInsuficienteException;

/**
 * Interface que define o comportamento de um cartão de crédito
 */
public interface Cartao {

	/**
	 *  faz o levantamento de uma dada quantia
	 * @param quantia quantia a levantar
	 * @throws CartaoInativoException 
	 * @throws SaldoInsuficienteException 
	 */
	public void levanta( long quantia ) throws CartaoInativoException, SaldoInsuficienteException;
	
	/**
	 *  faz uma compra
	 * @param quantia quantia a gastar
	 * @throws QuantiaInvalidaException 
	 * @throws SaldoInsuficienteException 
	 * @throws CartaoInativoException 
	 * @throws CartaoBloqueadoException 
	 */
	public void compra( long quantia ) throws SaldoInsuficienteException, CartaoInativoException;
	
	/**
	 * retorna o plafond 
	 * @return o plafond 
	 */
	public long getPlafond();
	
	/**
	 * Altera o plafond do cartão
	 * @param plafond
	 */
	public void setPlafond( long plafond );
	
	/**
	 * Indica qual o valor que ainda está disponível no plafond
	 * @return o valor que ainda está disponível no plafond
	 */
	public long getPlafondDisponivel();
	
	/**
	 * devolve a dívida do cartão, isto é, quanto é que já gastou usando o cartão
	 * @return a dívida do cartão
	 */
	public long getDivida();
	
	/**
	 * define a dívida associada ao cartão, isto é, quanto é que já gastou usando o cartão
	 * @param divida a dívida associada ao cartão
	 */
	public void setDivida( long divida );

	/**
	 * indica o estado do cartão (true = ativo, false=desativo)
	 * @return o estado do cartão
	 */
	public boolean estaActivo();	
	
	/**
	 * define o estado do cartão  (true = ativo, false=desativo)
	 * @param activo novo estado do cartão
	 */
	public void setActivo( boolean activo );
	
	/**
	 * faz as contas no final do mês
	 * @throws CartaoInativoException 
	 */
	public void resetPlafond() throws CartaoInativoException;
	
	/** 
	 * devolde o id do cartão
	 * @return o id do cartão
	 */
	public int getId();

	Conta getConta();
}
