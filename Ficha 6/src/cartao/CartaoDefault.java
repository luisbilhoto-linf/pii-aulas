package cartao;

import banco.Conta;
import banco.QuantiaInvalidaException;
import banco.SaldoInsuficienteException;

public class CartaoDefault implements Cartao {
	private int id;
	private Conta conta;
	private long plafond;
	private long divida;
	private boolean activo;

	public CartaoDefault(int id, Conta conta, long plafond) {
		this.id = id;
		this.conta = conta;
		setPlafond(plafond);
	}

	@Override
	public void levanta(long quantia) throws CartaoInativoException, SaldoInsuficienteException {
		try {
			getConta().levantar(quantia);
		} catch (SaldoInsuficienteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void compra(long quantia) throws SaldoInsuficienteException, CartaoInativoException { }

	@Override
	public long getPlafond() {
		return plafond;
	}

	@Override
	public void setPlafond(long plafond) {
		if (plafond < 0)
			throw new QuantiaInvalidaException("plafond negativo");

		this.plafond = plafond;
	}

	@Override
	public long getPlafondDisponivel() {
		return getPlafond() - getDivida();
	}

	@Override
	public long getDivida() {
		return divida;
	}

	@Override
	public void setDivida(long divida) {
		this.divida = divida;
	}

	@Override
	public boolean estaActivo() {
		return activo;
	}

	@Override
	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	@Override
	public void resetPlafond() throws CartaoInativoException {
		checkAtivo();
		plafond = 0;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public Conta getConta() {
		return conta;
	}

	protected void gastaPlafond(long quantia) throws SaldoInsuficienteException {
		if (quantia < 0)
			throw new QuantiaInvalidaException("Quantia Negativa!");
		if (quantia > getPlafondDisponivel())
			throw new SaldoInsuficienteException("Plafond Não Disponível!");

		divida += quantia;
	}

	protected void checkAtivo() throws CartaoInativoException {
		if (!estaActivo()) {
			throw new CartaoInativoException("id " + getId());
		}
	}
}
