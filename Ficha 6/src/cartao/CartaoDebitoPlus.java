package cartao;

import banco.Conta;

public class CartaoDebitoPlus extends CartaoDefault {

	public CartaoDebitoPlus(int id, Conta conta, long plafond) {
		super(id, conta, plafond);
	}
	
	@Override
	public void levanta(long quantia) { }
	
	@Override
	public void compra(long quantia) { }
	
	@Override
	public void resetPlafond() { }
}
