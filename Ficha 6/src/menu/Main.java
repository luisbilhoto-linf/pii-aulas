package menu;

import java.io.*;
import javax.swing.JOptionPane;

import banco.*;
import cartao.*;

public class Main {

	public static void main(String[] args) {
		Banco b = new Banco();

		lerClientes(b, "contas.bnc");
		lerCartoes(b, "cartoes.bnc");

		// criar uma nova thread para ter várias janelas ao mesmo tempo
		Thread t1 = new Thread() {
			public void run() {
				MenuBanco ib = new MenuBanco(b, 50, 100, 550, 500);
				ib.menuPrincipal();
				gravarClientes(b, "contas_out.bnc");
				System.exit(0);
			}
		};
		Thread t2 = new Thread() {
			public void run() {
				MenuCliente ic = new MenuCliente(b, 600, 100, 550, 500);
				ic.menuPrincipal();
			}
		};
		t1.start();
		t2.start();
	}

	private static Conta lerConta(String linha) {
		String dados[] = linha.split(",");
		return new Conta(Integer.parseInt(dados[1]), Long.parseLong(dados[2]));
	}

	private static void lerClientes(Banco b, String nomeFich) {
		try {
			BufferedReader fin = new BufferedReader(new FileReader(nomeFich));

			String linha = null;
			while ((linha = fin.readLine()) != null) {
				if (linha.startsWith("$conta")) {
					b.addConta(lerConta(linha));
				}
			}

			fin.close();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Ficheiro das contas " + nomeFich + " não encontrado!");
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Erro na leitura do ficheiro das contas: " + nomeFich + "!");
		}
	}

	private static void gravarClientes(Banco b, String nomeFich) {
		try {
			PrintWriter fout = new PrintWriter(new BufferedWriter(new FileWriter(nomeFich)));

			for (Conta c : b.getContas()) {
				fout.write(String.format("$conta,%d,%d\r\n", c.getID(), c.getSaldo()));
			}

			fout.close();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Erro na escrita do ficheiro das contas: " + nomeFich + "!");
		}
	}

	private static void lerCartoes(Banco b, String nomeFich) {
		try {
			BufferedReader fin = new BufferedReader(new FileReader(nomeFich));
			// TODO fazer a leitura da informação
			fin.close();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Ficheiro das contas " + nomeFich + " não encontrado!");
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Erro na leitura do ficheiro das contas: " + nomeFich + "!");
		}
	}

	/**
	 * método que lê a info de um cartão de débito
	 * 
	 * @param b     o banco onde pertence o cartão
	 * @param linha a linha do ficheiro a processar
	 * @return o cartão criado
	 */
	private static Cartao lerDebito(Banco b, String linha) {
		String info[] = linha.split(",");
		int id = Integer.parseInt(info[1]);
		int idConta = Integer.parseInt(info[2]);
		return null;
	}

	/**
	 * método que lê a info de um cartão de débito plus
	 * 
	 * @param b     o banco onde pertence o cartão
	 * @param linha a linha do ficheiro a processar
	 * @return o cartão criado
	 */
	private static Cartao lerDebitoPlus(Banco b, String linha) {
		return null;
	}

	/**
	 * método que lê a info de um cartão de fim de Mês
	 * 
	 * @param b     o banco onde pertence o cartão
	 * @param linha a linha do ficheiro a processar
	 * @return o cartão criado
	 */
	private static Cartao lerFimMes(Banco b, String linha) {
		return null;
	}

	/**
	 * método que lê a info de um cartão de prestações
	 * 
	 * @param b     o banco onde pertence o cartão
	 * @param linha a linha do ficheiro a processar
	 * @return o cartão criado
	 */
	private static Cartao lerPrestacoes(Banco b, String linha) {
		return null;
	}
}