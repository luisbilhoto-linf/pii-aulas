package menu;

import banco.Banco;
import banco.Conta;
import cartao.*;
import consola.SConsola;

public class MenuBanco {

	private SConsola aConsola;
	private Banco    oBanco;

	public MenuBanco(Banco b, int x, int y, int comp, int alt) {
		aConsola = new SConsola("Banco", comp, alt);
		aConsola.setPosition(x, y);
		oBanco = b;
	}

	public void menuPrincipal() {
		String menu = "Menu principal do banco" + "\n\n\t1 - criar conta" + "\n\t2 - criar cartão"
				+ "\n\t3 - fim do mês" + "\n\n\t0 - terminar operações";

		char op;
		do {
			aConsola.clear();
			aConsola.println(menu);
			op = Character.toUpperCase(aConsola.readChar());
			switch (op) {
			case '1':
				criarConta();
				break;
			case '2':
				criarCartao();
				break;
			case '3':
				fimMes();
				break;
			}
		} while (op != '0');
		aConsola.close();
	}

	private void criarConta() {
		aConsola.clear();
		int id = 0;
		do {
			aConsola.println("Número da conta? ");
			id = aConsola.readInt();

			if (oBanco.getConta(id) != null) {
				id = 0;
				aConsola.println("Número já usado!!!");
			}
		} while (id == 0);

		long saldo = 0;
		do {
			aConsola.println("Saldo inicial? ");
			saldo = aConsola.readLong();
			if (saldo <= 0) {
				saldo = 0;
				aConsola.println("Saldo tem de ser positivo!!!");
			}
		} while (saldo == 0);

		oBanco.addConta(id, saldo);
	}

	private void criarCartao() {
		aConsola.clear();

		Conta cliente = null;
		do {
			aConsola.println("Número da conta? ");
			int id = aConsola.readInt();

			cliente = oBanco.getConta(id);
			if (cliente == null) {
				id = 0;
				aConsola.println("Conta inexistente!!!");
			}
		} while (cliente == null);

		int id = 0;
		do {
			aConsola.println("Número para o cartão? ");
			id = aConsola.readInt();

			if (oBanco.getCartao(id) != null) {
				id = 0;
				aConsola.println("Número já usado!!!");
			}
		} while (id == 0);
		Cartao card = null;
		char op;
		do {
			aConsola.println("Tipo de cartão? " + "\n\t1 - débito" + "\n\t2 - débito com plafond"
					+ "\n\t3 - débito fim mês" + "\n\t4 - débito prestações");
			op = aConsola.readChar();
		} while (op < '1' || op > '4');

		long plaf = 0; // plafond do cartão

		switch (op) {
		case '1':
			card = new CartaoDebito(id, cliente);
			break;
		case '2':
			plaf = pedirPlafond();
			card = new CartaoDebitoPlus(id, cliente, plaf);
			break;
		case '3':
			plaf = pedirPlafond();
			card = new CartaoFimMes(id, cliente, plaf);
			break;
		case '4':
			plaf = pedirPlafond();
			break;
		}

		oBanco.addCartao(card);
	}

	private long pedirPlafond() {
		aConsola.println("Plafond?");
		long quantia = aConsola.readLong();
		if (quantia <= 0) {
			aConsola.print("Plafond inválido! Tem de ser positivo!");
			aConsola.readLine();
			return 0;
		}
		
		return quantia;
	}

	private void fimMes() {
		aConsola.clear();
		// TODO fazer ou mandar fazer as actividades de encerramento do mês
		aConsola.println("Operações concluidas");
		aConsola.readLine();
	}

}
