package menu;

import banco.Banco;
import banco.Conta;
import banco.SaldoInsuficienteException;
import cartao.Cartao;
import cartao.CartaoInativoException;
import consola.SConsola;

public class MenuCliente {

	private SConsola aConsola;
	private Banco oBanco; 

	public MenuCliente( Banco b, int x, int y, int comp, int alt ){
		aConsola = new SConsola( "Cliente", comp, alt );
		aConsola.setPosition( x, y );
		oBanco = b;
	}

	public void menuPrincipal( ) {
		String menu = "Terminal de cliente " +
					  "\n\n\t1 - fazer compra" +
		                "\n\t2 - fazer levantamento" + 
		                "\n\t3 - fazer depósito" +
		                "\n\t4 - ver situação do cartão" +
		                "\n\t5 - ver situação da conta" + 
		                "\n\t";

		char op;
		do {
			aConsola.clear();
			aConsola.println( menu );
			op = Character.toUpperCase( aConsola.readChar() );
			switch( op ){
			case '1':
				fazerCompra();
				break;
				
			case '2':
				fazerLevantamento();
				break;
				
			case '3':
				fazerDeposito();
				break;
				
			case '4':
				verCartao();
				break;
				
			case '5':
				verConta();
				break;
			}
		} while( true  );
	}
	
	private void fazerCompra() {
		aConsola.clear();
		
		Cartao card = pedirCartao();
		if( card == null ) return;
		
		long quantia = pedirQuantia();
		if( quantia == 0 ) return;

		try {
			card.compra(quantia);
			aConsola.println("Compra efectuada!");
			aConsola.readLine();
		} catch (SaldoInsuficienteException | CartaoInativoException e) {
			aConsola.println("Compra recusada!");
			aConsola.readLine();
		}		
	}
	
	private Cartao pedirCartao( ){
		aConsola.println( "Número do cartão? " );
		int id = aConsola.readInt();
		
		Cartao card = oBanco.getCartao(id);
		if( card == null ) {
			aConsola.print("Esse cartão não existe");
			aConsola.readLine();
//			return null;
		}

		return card;
	}
	
	private long pedirQuantia(){
		aConsola.println("Valor?");
		long quantia = aConsola.readLong();
		if( quantia <= 0 ){
			aConsola.print("Quantia inválida!");
			aConsola.readLine();
			return 0;
		}
		
		return quantia;
	}

	private void fazerLevantamento() {
		aConsola.clear();
		Cartao card = pedirCartao();
		if( card == null ) return;
		
		long quantia = pedirQuantia();
		if( quantia == 0 ) return;
		
		try {
			card.levanta(quantia);
			aConsola.println("Levantamento efectuado!");
			aConsola.readLine();		
		} catch (CartaoInativoException | SaldoInsuficienteException e) {
			aConsola.println("Levantamento recusado!");
			aConsola.readLine();		
		}		
	}
	
	private void fazerDeposito() {
		aConsola.clear();
		Conta c = pedirConta();
		if( c == null ) return;

		long quantia = pedirQuantia();
		if( quantia == 0 ) return;
		
		c.depositar(quantia);

		aConsola.println("Operação efectuada");				 
	    aConsola.readLine();
	}
	
	private Conta pedirConta(){
		aConsola.println( "Número da conta? " );
		Conta c = oBanco.getConta(aConsola.readInt());
		
		if( c == null ) {
			aConsola.print("Essa conta não existe");
			aConsola.readLine();
			return null;
		}
		return c;		
	}
	
	private void verConta() {
		aConsola.clear();
		Conta c = pedirConta();
		if( c == null ) return;
		
		aConsola.clear();
		aConsola.println( "Situação da conta " + c.getID() );
		aConsola.println( "  Saldo: " + c.getSaldo() );
		aConsola.println( "\nCartões associados à conta" );
		aConsola.println( "Número    Plafond  Disponível"); 
		for( Cartao cc : c.getCartoes() ){
			aConsola.println( String.format( "%6d %10d  %10d", cc.getId(), cc.getPlafond(), cc.getPlafondDisponivel() ) );
		}
		aConsola.readLine();
	}

	private void verCartao() {
		aConsola.clear();
		Object c = pedirCartao();
		if( c == null ) return;
		
		aConsola.clear();
		aConsola.println("Situação do cartão " + "NÚMERO" );
		aConsola.println("  Plafond " + "PLAFOND" );
		aConsola.println("  Divida  " + "DIVIDA" );
		aConsola.readLine();
	}
}