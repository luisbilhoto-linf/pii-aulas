package banco;

@SuppressWarnings("serial")
public class QuantiaInvalidaException extends RuntimeException {

	public QuantiaInvalidaException() { }

	public QuantiaInvalidaException(String message) {
		super(message);
	}

	public QuantiaInvalidaException(Throwable cause) {
		super(cause);
	}

	public QuantiaInvalidaException(String message, Throwable cause) {
		super(message, cause);
	}

	public QuantiaInvalidaException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
