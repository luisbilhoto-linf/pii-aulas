package banco;

import java.util.*;

import cartao.Cartao;

public class Banco {

	/**
	 * 
	 * criar um mapa que faça a ligação entre o id da conta e a conta criar um mapa
	 * que faça a ligação entre o id do cartão e o cartão Substituir o Object pelo
	 * tipo certo
	 * 
	 */
	private Map<Integer, Conta> contas = new HashMap<Integer, Conta>();
	private Map<Integer, Cartao> cartoes = new HashMap<Integer, Cartao>();

	public Banco() {
	}

	public Conta getConta(int id) {
		return contas.get(id);
	}

	public Collection<Conta> getContas() {
		return Collections.unmodifiableCollection(contas.values());
	}

	public void addConta(int id, long saldo) {
		addConta(new Conta(id, saldo));
	}

	public void addConta(Conta conta) {
		contas.put(conta.getID(), conta);
	}

	public void delConta(Conta conta) {
		delConta(conta.getID());
	}

	public void delConta(int id) {
		contas.remove(id);
	}

	public Cartao getCartao(int id) {
		return cartoes.get(id);
	}
	
	public Collection<Object> getCartoes() {
		return Collections.unmodifiableCollection(cartoes.values());
	}

	public void addCartao(Cartao cartao) {
		cartoes.put(cartao.getId(), cartao);
		cartao.getConta().addCartao(cartao);
	}

	public void delCartao(Cartao cartao) {
		delCartao(cartao.getId());
	}

	public void delCartao(int id) {
		cartoes.remove(id);
	}
}
