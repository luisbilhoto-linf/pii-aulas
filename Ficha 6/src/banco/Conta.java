package banco;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cartao.Cartao;

public class Conta {
	private int id;
	private long saldo;

	private List<Cartao> cartoes = new ArrayList<Cartao>();

	public Conta(int id, long saldo) {
		setID(id);
		setSaldo(saldo);
	}

	public int getID() {
		return id;
	}

	private void setID(int id) {
		this.id = id;
	}

	public long getSaldo() {
		return saldo;
	}

	private void setSaldo(long saldo) {
		if (saldo < 0) {
			throw new QuantiaInvalidaException("Saldo Negativo!");
		}

		this.saldo = saldo;
	}

	public List<Cartao> getCartoes() {
		return Collections.unmodifiableList(cartoes);
	}

	public void addCartao(Cartao cartao) {
		cartoes.add(cartao);
	}

	public void delCartao(Cartao cartao) {
		cartoes.remove(cartao);
	}

	public void delCartao(int pos) {
		cartoes.remove(pos);
	}

	public void depositar(long quantia) {
		if (quantia < 0)
			throw new QuantiaInvalidaException("Quantia a Depositar Negativa!");

		saldo += quantia;
	}

	public void levantar(long quantia) throws SaldoInsuficienteException {
		if (quantia < 0)
			throw new QuantiaInvalidaException("Quantia a Levantar Negativa!");
		if (quantia > saldo)
			throw new SaldoInsuficienteException("Quantia superior ao saldo!");

		saldo -= quantia;
	}

	@Override
	public String toString() {
		return String.format("Conta [id=%d, saldo=%d€]", id, saldo);
	}
}
