package ex3;

public class Elevador {
	private int ainferior;
	private int asuperior;
	private int aatual;

	/**
	 * Construtor da classe 'Elevador'
	 * 
	 * @param aa Inteiro do Andar Atual
	 * @param ai Inteiro do Andar Inferior
	 * @param as Inteiro do Andar Superior
	 */
	public Elevador(int aa, int ai, int as) {
		setAndarInferior(ai);
		setAndarSuperior(as);
		setAndarAtual(aa);
	}

	/**
	 * Construtor da classe 'Elevador'
	 * 
	 * @param ai Inteiro do Andar Inferior
	 * @param as Inteiro do Andar Superior
	 */
	public Elevador(int ai, int as) {
		setAndarInferior(ai);
		setAndarSuperior(as);
		setAndarAtual(ai);
	}

	/**
	 * Construtor da classe 'Elevador'
	 * 
	 * @param as Inteiro do Andar Superior
	 */
	public Elevador(int as) {
		setAndarInferior(0);
		setAndarSuperior(as);
		setAndarAtual(0);
	}

	public int getAndarInferior() {
		return ainferior;
	}

	public void setAndarInferior(int ai) {
		if (ai <= getAndarSuperior()) {
			this.ainferior = ai;
			if (getAndarAtual() < ai) {
				this.aatual = ai;
			}
		}
	}

	public int getAndarSuperior() {
		return asuperior;
	}

	public void setAndarSuperior(int as) {
		if (as >= getAndarInferior()) {
			this.asuperior = as;
			if (getAndarAtual() > as) {
				this.aatual = as;
			}
		}
	}

	public int getAndarAtual() {
		return aatual;
	}

	public void setAndarAtual(int aa) {
		if (acede(aa)) {
			this.aatual = aa;
		}
	}

	public boolean isTopo() {
		return getAndarAtual() == getAndarSuperior();
	}

	public boolean isFundo() {
		return getAndarAtual() == getAndarInferior();
	}

	public boolean acede(int andar) {
		return andar >= getAndarInferior() && andar <= getAndarSuperior();
	}

	public void doSubir() {
		if (!isTopo()) {
			setAndarAtual(getAndarAtual() + 1);
		}
	}

	public void doDescer() {
		if (!isFundo()) {
			setAndarAtual(getAndarAtual() - 1);
		}
	}

	public void irPara(int andar) {
		if (acede(andar)) {
			while (andar < getAndarAtual()) {
				doDescer();
			}
			while (andar > getAndarAtual()) {
				doSubir();
			}
		}
	}

	@Override
	public String toString() {
		return String.format("Elevador [Andar Atual: '%d', Andar Inferior: '%d', Andar Superior: '%d']", getAndarAtual(),
				getAndarInferior(), getAndarSuperior());
	}
}
