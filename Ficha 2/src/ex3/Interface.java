package ex3;

import java.util.Vector;
import java.util.Scanner;
import java.util.InputMismatchException;

public class Interface {
	private static Vector<Elevador> elevador = new Vector<Elevador>();
	private static Scanner input = new Scanner(System.in);

	/**
	 * inputChar
	 * 
	 * Este m�todo retorna um caractere. Caso na linha n�o seja possivel encontrar
	 * nenhum caracter, este retorna (char)13, ou seja o caracter da tecla 'ENTER'.
	 * 
	 * @param input Scanner declarado em main;
	 */
	private static char inputChar() {
		return (input.nextLine() + (char) 13).charAt(0);
	}

	/**
	 * inputString
	 * 
	 * Este m�todo retorna uma linha em String. Caso existam espa�os ou caracteres
	 * 'brancos' desnecess�rios nesta, estes s�o apagados e a linha limpa para uso.
	 * 
	 * @param text Mensagem String a apresentar antes do scanner;
	 */
	private static String inputString(String text) {
		System.out.print(text);
		return input.nextLine().trim();
	}

	/**
	 * inputInt
	 * 
	 * Este m�todo retorna um n�mero inteiro. Possui um limitador m�nimo bem como
	 * uma mensagem de erro e de texto antes do scanner. Uso do 'try catch' para
	 * evitar crashs na aplica��o.
	 * 
	 * @param text  Mensagem String a apresentar antes do scanner;
	 * @param error Mensagem String de erro;
	 * @param min   Delimitador m�nimo;
	 */
	private static int inputInt(String text, String error, int min) {
		int inputInt = min;
		do {
			if (inputInt < min) {
				System.out.println("\n" + error);
			}
			System.out.print(text);
			try {
				inputInt = input.nextInt();
			} catch (InputMismatchException e) {
				inputInt = min - 1;
				input.nextLine(); // System.in junk reset
			}
		} while (inputInt < min);
		input.nextLine(); // System.in junk reset
		return inputInt;
	}

	/**
	 * inputInt (overloading)
	 * 
	 * Este m�todo retorna um n�mero inteiro. Possui um limitador m�nimo e m�ximo
	 * bem como uma mensagem de erro e de texto antes do scanner. Uso do 'try catch'
	 * para evitar crashs na aplica��o.
	 * 
	 * @param text  Mensagem String a apresentar antes do scanner;
	 * @param error Mensagem String de erro;
	 * @param min   Delimitador m�nimo;
	 * @param max   Delimitador m�ximo;
	 */
	private static int inputInt(String text, String error, int min, int max) {
		int inputInt = min;
		do {
			if (inputInt < min || inputInt > max) {
				System.out.println("\n" + error);
			}
			System.out.print(text);
			try {
				inputInt = input.nextInt();
			} catch (InputMismatchException e) {
				inputInt = min - 1;
				input.nextLine(); // System.in junk reset
			}
		} while (inputInt < min || inputInt > max);
		input.nextLine(); // System.in junk reset
		return inputInt;
	}

	/**
	 * printWait
	 * 
	 * Este m�todo imprime uma mensagem de espera e sucessivamente espera pelo input
	 * de qualquer coisa. Funcionalidade de embelezamento e melhorias de
	 * usabilidade.
	 * 
	 * @param input Scanner declarado em main;
	 */
	private static void printWait() {
		System.out.println("\nPressione a tecla ENTER para continuar...");
		input.nextLine(); // Wait until user press ENTER key.
	}

	public static void main(String[] args) {
		elevador.add(new Elevador(5, -3, 10));
		elevador.add(new Elevador(-1, -1, 12));
		elevador.add(new Elevador(0, 0, 4));

		char menu = 'S';
		do {
			System.out.println("MENU PRINCIPAL");
			System.out.println("A - Mover o 1� elevador");
			System.out.println("B - Mover o 2� elevador");
			System.out.println("C - Mover o 3� elevador");
			System.out.println("D - Informa��es dos elevador");
			System.out.println("S - Sair");
			menu = Character.toUpperCase(inputChar());
			System.out.print("\n");
			switch (menu) {
			case 'A':
				System.out.print("Introduza o andar para ir: ");
				elevador.get(0).irPara(input.nextInt());
				break;
			case 'B':
				System.out.print("Introduza o andar para ir: ");
				elevador.get(1).irPara(input.nextInt());
				break;
			case 'C':
				System.out.print("Introduza o andar para ir: ");
				elevador.get(2).irPara(input.nextInt());
				break;
			case 'D':
				for (int i = 0; i < elevador.size(); i++) {
					System.out.printf("%d %s", (i + 1), elevador.get(i).toString());
					System.out.print("\n");
				}
				break;
			case 'S':
				input.close();
				return;

			default:
				System.out.printf("Op��o '%s' n�o reconhecida! Tente novamente.\n", menu == 13 ? "ENTER" : menu);
				break;
			}
			printWait();
		} while (menu != 'S');
	}
}
