package ex1n2;

import java.util.Vector;
import java.util.Scanner;
import java.util.InputMismatchException;

public class Interface {
	private static Vector<Pessoa> pessoas = new Vector<Pessoa>();
	private static Scanner input = new Scanner(System.in);

	/**
	 * inputChar
	 * 
	 * Este m�todo retorna um caractere. Caso na linha n�o seja possivel encontrar
	 * nenhum caracter, este retorna (char)13, ou seja o caracter da tecla 'ENTER'.
	 * 
	 * @param input Scanner declarado em main;
	 */
	private static char inputChar() {
		return (input.nextLine() + (char) 13).charAt(0);
	}

	/**
	 * inputString
	 * 
	 * Este m�todo retorna uma linha em String. Caso existam espa�os ou caracteres
	 * 'brancos' desnecess�rios nesta, estes s�o apagados e a linha limpa para uso.
	 * 
	 * @param text Mensagem String a apresentar antes do scanner;
	 */
	private static String inputString(String text) {
		System.out.print(text);
		return input.nextLine().trim();
	}

	/**
	 * inputInt
	 * 
	 * Este m�todo retorna um n�mero inteiro. Possui um limitador m�nimo bem como
	 * uma mensagem de erro e de texto antes do scanner. Uso do 'try catch' para
	 * evitar crashs na aplica��o.
	 * 
	 * @param text  Mensagem String a apresentar antes do scanner;
	 * @param error Mensagem String de erro;
	 * @param min   Delimitador m�nimo;
	 */
	private static int inputInt(String text, String error, int min) {
		int inputInt = min;
		do {
			if (inputInt < min) {
				System.out.println("\n" + error);
			}
			System.out.print(text);
			try {
				inputInt = input.nextInt();
			} catch (InputMismatchException e) {
				inputInt = min - 1;
				input.nextLine(); // System.in junk reset
			}
		} while (inputInt < min);
		input.nextLine(); // System.in junk reset
		return inputInt;
	}

	/**
	 * inputInt (overloading)
	 * 
	 * Este m�todo retorna um n�mero inteiro. Possui um limitador m�nimo e m�ximo
	 * bem como uma mensagem de erro e de texto antes do scanner. Uso do 'try catch'
	 * para evitar crashs na aplica��o.
	 * 
	 * @param text  Mensagem String a apresentar antes do scanner;
	 * @param error Mensagem String de erro;
	 * @param min   Delimitador m�nimo;
	 * @param max   Delimitador m�ximo;
	 */
	private static int inputInt(String text, String error, int min, int max) {
		int inputInt = min;
		do {
			if (inputInt < min || inputInt > max) {
				System.out.println("\n" + error);
			}
			System.out.print(text);
			try {
				inputInt = input.nextInt();
			} catch (InputMismatchException e) {
				inputInt = min - 1;
				input.nextLine(); // System.in junk reset
			}
		} while (inputInt < min || inputInt > max);
		input.nextLine(); // System.in junk reset
		return inputInt;
	}

	private static void printTelefonePorNome(String n) {
		for (int i = 0; i < pessoas.size(); i++) {
			Pessoa p = pessoas.get(i);
			if (p.getNome().equalsIgnoreCase(n)) {
				System.out.printf("O n�mero de telefone � %s.\n", p.getTelefone());
				return;
			}
		}
		System.out.println("O nome introduzido n�o se encontra guardado!");
	}

	private static void printNomeMoradaPorTelefone(String t) {
		for (int i = 0; i < pessoas.size(); i++) {
			Pessoa p = pessoas.get(i);
			if (p.getTelefone().equals(t)) {
				System.out.printf("Nome: %s\n", p.getNome());
				System.out.printf("Morada: %s\n", p.getMorada());
				return;
			}
		}
		System.out.println("O telefone introduzido n�o se encontra guardado!");
	}

	/**
	 * printWait
	 * 
	 * Este m�todo imprime uma mensagem de espera e sucessivamente espera pelo input
	 * de qualquer coisa. Funcionalidade de embelezamento e melhorias de
	 * usabilidade.
	 * 
	 * @param input Scanner declarado em main;
	 */
	private static void printWait() {
		System.out.println("\nPressione a tecla ENTER para continuar...");
		input.nextLine(); // Wait until user press ENTER key.
	}

	public static void main(String[] args) {
		pessoas.add(new Pessoa("Lu�s Bilhoto 1", "Rua da Capela 3, Alij�", "5070-035", "+351 935134442", "28/02/1998",
				's'));
		pessoas.add(new Pessoa("Lu�s Bilhoto 2", "Rua da Capela 3, Alij�", "5070-035", "+351 935134442", "28/02/1998",
				's'));
		pessoas.add(new Pessoa("Lu�s Bilhoto 3", "Rua da Capela 3, Alij�", "5070-035", "+351 935134442", "28/02/1998",
				'c'));
		pessoas.add(new Pessoa("Lu�s Bilhoto 4", "Rua da Capela 3, Alij�", "5070-035", "+351 935134442", "28/02/1998",
				'c'));
		pessoas.add(new Pessoa("Lu�s Bilhoto 5", "Rua da Capela 3, Alij�", "5070-035", "+351 935134442", "28/02/1998",
				'v'));

		char menu = 'S';
		do {
			System.out.println("MENU PRINCIPAL");
			System.out.println("A - Pessoas Maiores de Idade");
			System.out.println("B - Pessoas Por Estado Civil");
			System.out.println("C - Pessoas Por C�digo Postal");
			System.out.println("D - Pessoa Por Nome");
			System.out.println("E - Pessoa Por Telefone");
			System.out.println("S - Sair");
			menu = Character.toUpperCase(inputChar());
			System.out.print("\n");
			switch (menu) {
			case 'A':
				int aRef = inputInt("Ano de Refer�ncia: ", "Ano introduzido inv�lido! (0000-9999)", 0000, 9999);
				for (int i = 0; i < pessoas.size(); i++) {
					Pessoa p = pessoas.get(i);
					if (p.isMaiorIdade(aRef)) {
						System.out.printf("\n%s\n", p.toString());
					}
				}
				break;
			case 'B':
				System.out.print("Estado Civil: ");
				char eCivil = inputChar();
				for (int i = 0; i < pessoas.size(); i++) {
					Pessoa p = pessoas.get(i);
					if (p.getECivil() == eCivil) {
						System.out.printf("\n%s\n", p.toString());
					}
				}
				break;
			case 'C':
				String postal = inputString("C�digo Postal: ");
				for (int i = 0; i < pessoas.size(); i++) {
					Pessoa p = pessoas.get(i);
					if (p.getPostal().equals(postal)) {
						System.out.printf("\n%s\n", p.toString());
					}
				}
				break;
			case 'D':
				printTelefonePorNome(inputString("Nome: "));
				break;
			case 'E':
				printNomeMoradaPorTelefone(inputString("Telefone: "));
				break;
			case 'S':
				input.close();
				return;

			default:
				System.out.printf("Op��o '%s' n�o reconhecida! Tente novamente.\n", menu == 13 ? "ENTER" : menu);
				break;
			}
			printWait();
		} while (menu != 'S');
	}
}
