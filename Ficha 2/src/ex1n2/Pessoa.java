package ex1n2;

public class Pessoa {
	private String nome;
	private String morada;
	private String postal;
	private String telefone;
	private String nascimento;
	private char ecivil;

	/**
	 * Construtor da Classe 'Pessoa'
	 * 
	 * @param nome
	 * @param morada
	 * @param postal
	 * @param telefone
	 * @param nascimento
	 * @param ecivil
	 */
	public Pessoa(String nome, String morada, String postal, String telefone, String nascimento, char ecivil) {
		this.nome = nome;
		this.morada = morada;
		this.postal = postal;
		this.telefone = telefone;
		this.nascimento = nascimento;
		this.ecivil = ecivil;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMorada() {
		return morada;
	}

	public void setMorada(String morada) {
		this.morada = morada;
	}

	public String getPostal() {
		return postal;
	}

	public void setPostal(String postal) {
		this.postal = postal;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getNascimento() {
		return nascimento;
	}

	public void setNascimento(String nascimento) {
		this.nascimento = nascimento;
	}

	public int getAnoNascimento() {
		return Integer.parseInt(getNascimento().split("/")[2]);
	}

	public char getECivil() {
		return ecivil;
	}

	public void setECivil(char ecivil) {
		this.ecivil = ecivil;
	}

	public String getECivilString() {
		switch (getECivil()) {
		case 's':
			return "solteiro";
		case 'c':
			return "casado";
		case 'd':
			return "divorciado";
		case 'v':
			return "vi�vo";
		case 'u':
			return "uni�o de facto";
		default:
			return "desconhecido";
		}
	}

	public String toString() {
		return String.format("Nome: %s\nMorada: %s\nTelefone: %s\nData de nascimento: %s\nEstado civil: %s", getNome(),
				getMorada(), getTelefone(), getNascimento(), getECivilString());
	}

	public int getIdade(int anoRef) {
		return anoRef - getAnoNascimento();
	}

	public boolean isMaiorIdade(int anoRef) {
		return getIdade(anoRef) >= 18;
	}
}
