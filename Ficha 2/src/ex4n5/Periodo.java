package ex4n5;

public class Periodo {
	private Hora hInicio;
	private Hora hFim;

	public Periodo(Hora hInicio, Hora hFim) {
		setHoraInicio(hInicio);
		setHoraFim(hFim);
	}

	public Periodo(Hora hInicio, int segundos) {
		setHoraInicio(hInicio);
		setHoraFim(hInicio.clone());
		getHoraFim().somaSegundos(segundos);
	}

	public Hora getHoraInicio() {
		return hInicio;
	}

	public void setHoraInicio(Hora hInicio) {
		this.hInicio = hInicio;
	}

	public Hora getHoraFim() {
		return hFim;
	}

	public void setHoraFim(Hora hFim) {
		this.hFim = hFim;
	}

	public long duracao() {
		return getHoraFim().getDiferencaSegs(getHoraInicio());
	}

	public boolean estaDentro(Hora outra) {
		long d = getHoraFim().getDiferencaSegs(outra);
		return d >= 0 && d <= duracao();
	}
	
//	public boolean interseta(Periodo outro) {
//		return estaDentro(outro.getHoraInicio()) || estaDentro(outro.getHoraFim());
//	}

	public int interseta(Periodo outro) {
		if (estaDentro(outro.getHoraFim()) && !estaDentro(outro.getHoraInicio())) {
			return -1;
		}
		if (estaDentro(outro.getHoraInicio()) && !estaDentro(outro.getHoraFim())) {
			return 1;
		}
		return 0;
	}

	public boolean estaContido(Periodo outro) {
		return estaDentro(outro.getHoraInicio()) && estaDentro(outro.getHoraFim());
	}

	public boolean contem(Periodo outro) {
		return outro.estaContido(this);
	}

	public Periodo getUniao(Periodo outro) {
		if (interseta(outro) == -1) {
			return new Periodo(outro.getHoraInicio().clone(), getHoraFim().clone());
		}
		if (interseta(outro) == 1) {
			return new Periodo(getHoraInicio().clone(), outro.getHoraFim().clone());
		}
		if (contem(outro)) {
			return new Periodo(outro.getHoraInicio().clone(), outro.getHoraFim().clone());
		}
		return new Periodo(getHoraInicio().clone(), getHoraFim().clone());
	}

	public Periodo getIntersecao(Periodo outro) {
		if (interseta(outro) == -1) {
			return new Periodo(getHoraInicio().clone(), outro.getHoraFim().clone());
		}
		if (interseta(outro) == 1) {
			return new Periodo(outro.getHoraInicio().clone(), getHoraFim().clone());
		}
		return null;
	}

	public void junta(Periodo outro) {
		if (interseta(outro) != 0) {
			Periodo p = getUniao(outro);
			setHoraInicio(p.getHoraInicio());
			setHoraFim(p.getHoraFim());
		}
	}

	@Override
	public String toString() {
		return String.format("%s - %s", getHoraInicio().toString(), getHoraFim().toString());
	}
}
