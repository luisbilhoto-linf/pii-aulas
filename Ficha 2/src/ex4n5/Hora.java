package ex4n5;

import java.util.Calendar;

public class Hora {
	private int horas;
	private int minutos;
	private int segundos;

	public Hora(int h, int m, int s) {
		setHoras(h);
		setMinutos(m);
		setSegundos(s);
	}

	public Hora() {
		Calendar cal = Calendar.getInstance();

		setHoras(cal.get(Calendar.HOUR_OF_DAY));
		setMinutos(cal.get(Calendar.MINUTE));
		setSegundos(cal.get(Calendar.SECOND));
	}

	public Hora(long sMeiaNoite) {
		sMeiaNoite = sMeiaNoite >= 0 ? sMeiaNoite : 0;
		sMeiaNoite = sMeiaNoite <= 86400 ? sMeiaNoite : 86400;

		setHoras((int) (sMeiaNoite / 3600));
		setMinutos((int) ((sMeiaNoite % 3600) / 60));
		setSegundos((int) ((sMeiaNoite % 3600) % 60));
	}

	public Hora(String hStr) {
		String str[] = hStr.split(":");

		setHoras(Integer.parseInt(str[0]));
		setMinutos(Integer.parseInt(str[1]));
		setSegundos(Integer.parseInt(str[2]));
	}

	public int getHoras() {
		return horas;
	}

	public void setHoras(int horas) {
		this.horas = horas > 0 && horas < 24 ? horas : 0;
	}

	public int getMinutos() {
		return minutos;
	}

	public void setMinutos(int minutos) {
		this.minutos = minutos > 0 && minutos < 60 ? minutos : 0;
	}

	public int getSegundos() {
		return segundos;
	}

	public void setSegundos(int segundos) {
		this.segundos = segundos > 0 && segundos < 60 ? segundos : 0;
	}

	public int somaHoras(int horas) {
		horas += getHoras();
		setHoras(horas % 24);
		return horas;
	}

	public int somaMinutos(int minutos) {
		minutos += getMinutos();
		somaHoras(minutos / 60);
		setMinutos(minutos % 60);
		return minutos;
	}

	public int somaSegundos(int segundos) {
		segundos += getSegundos();
		somaMinutos(segundos / 60);
		setSegundos(segundos % 60);
		return segundos;
	}

	public int[] compareTo(Hora outra) {
		return new int[] { getHoras() != outra.getHoras() ? getHoras() - outra.getHoras() : 0,
				getMinutos() != outra.getMinutos() ? getMinutos() - outra.getMinutos() : 0,
				getSegundos() != outra.getSegundos() ? getSegundos() - outra.getSegundos() : 0 };
	}

	public long getDiferencaSegs(Hora outra) {
		int[] c = compareTo(outra);
		return c[0] * 3600 + c[1] * 60 + c[2];
	}

	public long toSegundos() {
		return getHoras() * 3600 + getMinutos() * 60 + getSegundos();
	}

	@Override
	public String toString() {
		return String.format("%02d:%02d:%02d", getHoras(), getMinutos(), getSegundos());
	}

	@Override
	public Hora clone() {
		return new Hora(getHoras(), getMinutos(), getSegundos());
	}
}
