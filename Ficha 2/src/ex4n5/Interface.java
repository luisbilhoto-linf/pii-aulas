package ex4n5;

import java.util.Vector;
import java.util.Scanner;
import java.util.InputMismatchException;

public class Interface {
	private static Vector<Periodo> periodos = new Vector<Periodo>();
	private static Scanner input = new Scanner(System.in);

	/**
	 * inputChar
	 * 
	 * Este m�todo retorna um caractere. Caso na linha n�o seja possivel encontrar
	 * nenhum caracter, este retorna (char)13, ou seja o caracter da tecla 'ENTER'.
	 * 
	 * @param input Scanner declarado em main;
	 */
	private static char inputChar() {
		return (input.nextLine() + (char) 13).charAt(0);
	}

	/**
	 * inputString
	 * 
	 * Este m�todo retorna uma linha em String. Caso existam espa�os ou caracteres
	 * 'brancos' desnecess�rios nesta, estes s�o apagados e a linha limpa para uso.
	 * 
	 * @param text Mensagem String a apresentar antes do scanner;
	 */
	private static String inputString(String text) {
		System.out.print(text);
		return input.nextLine().trim();
	}

	/**
	 * inputInt
	 * 
	 * Este m�todo retorna um n�mero inteiro. Possui um limitador m�nimo bem como
	 * uma mensagem de erro e de texto antes do scanner. Uso do 'try catch' para
	 * evitar crashs na aplica��o.
	 * 
	 * @param text  Mensagem String a apresentar antes do scanner;
	 * @param error Mensagem String de erro;
	 * @param min   Delimitador m�nimo;
	 */
	private static int inputInt(String text, String error, int min) {
		int inputInt = min;
		do {
			if (inputInt < min) {
				System.out.println("\n" + error);
			}
			System.out.print(text);
			try {
				inputInt = input.nextInt();
			} catch (InputMismatchException e) {
				inputInt = min - 1;
				input.nextLine(); // System.in junk reset
			}
		} while (inputInt < min);
		input.nextLine(); // System.in junk reset
		return inputInt;
	}

	/**
	 * inputInt (overloading)
	 * 
	 * Este m�todo retorna um n�mero inteiro. Possui um limitador m�nimo e m�ximo
	 * bem como uma mensagem de erro e de texto antes do scanner. Uso do 'try catch'
	 * para evitar crashs na aplica��o.
	 * 
	 * @param text  Mensagem String a apresentar antes do scanner;
	 * @param error Mensagem String de erro;
	 * @param min   Delimitador m�nimo;
	 * @param max   Delimitador m�ximo;
	 */
	private static int inputInt(String text, String error, int min, int max) {
		int inputInt = min;
		do {
			if (inputInt < min || inputInt > max) {
				System.out.println("\n" + error);
			}
			System.out.print(text);
			try {
				inputInt = input.nextInt();
			} catch (InputMismatchException e) {
				inputInt = min - 1;
				input.nextLine(); // System.in junk reset
			}
		} while (inputInt < min || inputInt > max);
		input.nextLine(); // System.in junk reset
		return inputInt;
	}

	/**
	 * printWait
	 * 
	 * Este m�todo imprime uma mensagem de espera e sucessivamente espera pelo input
	 * de qualquer coisa. Funcionalidade de embelezamento e melhorias de
	 * usabilidade.
	 * 
	 * @param input Scanner declarado em main;
	 */
	private static void printWait() {
		System.out.println("\nPressione a tecla ENTER para continuar...");
		input.nextLine(); // Wait until user press ENTER key.
	}

	public static void main(String[] args) {
		periodos.add(new Periodo(new Hora(), 7200));
		periodos.add(new Periodo(new Hora(0), 14400));
		periodos.add(new Periodo(new Hora("10:30:00"), 7200));
		periodos.add(new Periodo(new Hora(11, 30, 0), 7200));
		periodos.add(new Periodo(new Hora(12, 30, 0), new Hora(14, 30, 0)));

		char menu = 'S';
		do {
			System.out.println("MENU PRINCIPAL");
			System.out.println("A - Mudar hora de in�cio de um periodo");
			System.out.println("B - Mudar hora de fim de um periodo");
			System.out.println("C - Informa��es dos periodos");
			System.out.println("D - Juntar dois periodos");
			System.out.println("S - Sair");
			menu = Character.toUpperCase(inputChar());
			System.out.print("\n");
			switch (menu) {
			case 'A':
				periodos.get(
						(inputInt("N�mero do periodo: ", "N�mero do periodo inv�lido!", 1, (periodos.size() + 1)) - 1))
						.setHoraInicio(new Hora(inputString("Nova hora de in�cio (hh:mm:ss): ")));
				;
				break;
			case 'B':
				periodos.get(
						(inputInt("N�mero do periodo: ", "N�mero do periodo inv�lido!", 1, (periodos.size() + 1)) - 1))
						.setHoraFim(new Hora(inputString("Nova hora de fim (hh:mm:ss): ")));
				;
				break;
			case 'C':
				for (int i = 0; i < periodos.size(); i++) {
					System.out.printf("%d: %s", (i + 1), periodos.get(i).toString());
					System.out.print("\n");
				}
				printWait();
				break;
			case 'D':
				periodos.get(
						(inputInt("N�mero do 1� periodo: ", "N�mero do periodo inv�lido!", 1, (periodos.size() + 1)) - 1))
						.junta(periodos.get((inputInt("N�mero do 2� periodo: ", "N�mero do periodo inv�lido!", 1, (periodos.size() + 1)) - 1)));
				break;
			case 'S':
				input.close();
				return;

			default:
				System.out.printf("Op��o '%s' n�o reconhecida! Tente novamente.\n", menu == 13 ? "ENTER" : menu);
				printWait();
				break;
			}
		} while (menu != 'S');
	}
}
