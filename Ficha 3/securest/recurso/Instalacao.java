package securest.recurso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import p2.tempo.Hora;
import p2.tempo.Horario;

public class Instalacao {
	private int id;
	private String info;
	private String nome;
	private Horario horario;
	private int acesso;

	// Lista de funcionarios autorizados e presentes
	private List<Funcionario> autorizados = new ArrayList<Funcionario>();
	private List<Funcionario> presentes = new ArrayList<Funcionario>();

	public Instalacao(int id, String nome, String info, int nivel, Horario horario, List<Funcionario> autorizados) {
		setID(id);
		setNome(nome);
		setInfo(info);
		setAcesso(nivel);
		setHorario(horario);
		this.autorizados = autorizados;
	}

	public Instalacao(int id, String nome, String info, int nivel, Horario horario) {
		setID(id);
		setNome(nome);
		setInfo(info);
		setAcesso(nivel);
		setHorario(horario);
	}

	public Instalacao(int id, String nome, String info, int nivel) {
		setID(id);
		setNome(nome);
		setInfo(info);
		setAcesso(nivel);
	}

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Horario getHorario() {
		return horario;
	}

	public void setHorario(Horario horario) {
		this.horario = horario;
	}

	public int getAcesso() {
		return acesso;
	}

	public void setAcesso(int nivel) {
		this.acesso = nivel > 0 && nivel <= 5 ? nivel : 1;
	}

	public List<Funcionario> getAutorizados() {
		return Collections.unmodifiableList(autorizados);
	}

	public void addAutorizados(Funcionario f) {
		autorizados.add(f);
	}

	public void removeAutorizado(Funcionario f) {
		autorizados.remove(f);
	}

	public List<Funcionario> getPresentes() {
		return Collections.unmodifiableList(presentes);
	}

	public boolean podeEntrar(Funcionario f, Hora h) {
		return getAcesso() <= f.getAcesso() && getHorario() != null && getHorario().estaDentro(h)
				|| autorizados.contains(f);
	}

	public boolean entrar(Funcionario f) {
		if (podeEntrar(f, new Hora())) {
			presentes.add(f);
			f.setLocal(this);
			return true;
		}
		return false;
	}

	public void sair(Funcionario f) {
		if (presentes.remove(f)) {
			f.sair();
		}
	}

	public boolean equals(Instalacao i) {
		return getID() == i.getID();
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		return this == obj && obj != null && getClass() == obj.getClass() && id == ((Instalacao) obj).id;
	}

	@Override
	public String toString() {
		return "Instalacao [id=" + id + ", info=" + info + ", nome=" + nome + ", horario=" + horario + ", acesso="
				+ acesso + ", autorizados=" + autorizados + ", presentes=" + presentes + "]";
	}
}
