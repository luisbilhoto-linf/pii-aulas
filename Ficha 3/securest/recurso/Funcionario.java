package securest.recurso;

import java.util.Objects;

public class Funcionario {
	private int id;
	private String nome;
	private Instalacao local;
	private int acesso;

	public Funcionario(int id, String nome, int nivel) {
		setID(id);
		setNome(nome);
		setAcesso(nivel);
	}

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	void setLocal(Instalacao local) {
		this.local = local;
	}

	public Instalacao getLocal() {
		return local;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getAcesso() {
		return acesso;
	}

	public void setAcesso(int nivel) {
		this.acesso = nivel > 0 && nivel <= 5 ? nivel : 1;
	}

	public boolean equals(Funcionario f) {
		return getID() == f.getID();
	}

	public boolean estaPresente() {
		return getLocal() != null;
	}

	public void sair() {
		setLocal(null);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		return this == obj && obj != null && getClass() == obj.getClass() && id == ((Funcionario) obj).id;
	}

	@Override
	public String toString() {
		return "Funcionario [id=" + id + ", nome=" + nome + ", local=" + local + ", acesso=" + acesso + "]";
	}
}
