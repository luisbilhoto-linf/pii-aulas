package securest.app;

import consola.SConsola;
import p2.tempo.Periodo;
import securest.recurso.CentralControlo;
import securest.recurso.Funcionario;
import securest.recurso.Instalacao;

/**
 * Classe que trata dos menus da central de comandos
 */
public class MenuCentral {

	private SConsola consola = new SConsola();
	private CentralControlo central;

	public MenuCentral(CentralControlo central) {
		this.central = central;
	}

	/**
	 * menu principal da aplicação
	 */
	public void menuPrincipal() {
		String menu = "Central de controlo - controlo de acessos\n" + "1- Info sobre instalação\n"
				+ "2- Info sobre funcionário\n" + "3- Entrada de funcionário\n" + "4- Saída de funcionário\n"
				+ "5- Listar presenças em todas as instalações\n" + "\n0- Sair";

		char opcao = 0;
		do {
			consola.clear();
			consola.println(menu);
			opcao = consola.readChar();
			switch (opcao) {
			case '1':
				printInstalacao();
				break;
			case '2':
				printFuncionario();
				break;
			case '3':
				entradaFuncionario();
				break;
			case '4':
				saidaFuncionario();
				break;
			case '5':
				listarPresencas();
				consola.readLine();
				break;
			case '0':
				break;
			default:
				consola.println("opção inválida");
				consola.readLine();
			}
		} while (opcao != '0');

		consola.close(); // desligar o menu da central
	}

	/**
	 * imprime a informação de uma instalação
	 */
	private void printInstalacao() {
		Instalacao inst = getInstalacao("Código da instalação? ");
		String delim;
		consola.clear();
		consola.println(inst.getNome());
		consola.println("id: " + inst.getID());
		consola.println("nivel acesso: " + inst.getAcesso());
		consola.print("Autorizados: ");
		delim = "";
		for (Funcionario a : inst.getAutorizados()) {
			consola.print(delim + a.getNome());
			delim = ", ";
		}
		consola.println();
		consola.print("Presentes: ");
		delim = "";
		for (Funcionario p : inst.getPresentes()) {
			consola.print(delim + p.getNome());
			delim = ", ";
		}
		consola.println();
		consola.println("Horário funcionamento");
		if (inst.getHorario() != null) {
			for (Periodo p : inst.getHorario().getPeriodos()) {
				consola.println(p.getInicio().toString() + " - " + p.getFim().toString());
			}
		}
		consola.readLine();
	}

	/**
	 * imprime a informação de um funcionário
	 */
	private void printFuncionario() {
		Funcionario f = getFuncionario("Código do funcionário? ");
		consola.clear();
		consola.println(f.getNome());
		consola.println("id: " + f.getID());
		consola.println("nivel acesso: " + f.getAcesso());
		if (f.estaPresente())
			consola.print("Presente em " + f.getLocal().getNome());
		else
			consola.print("Não está presente em nenhuma instalação");
		consola.readLine();
	}

	/**
	 * processa a entrada de um funcionário
	 */
	private void entradaFuncionario() {
		Funcionario f = getFuncionario("Código do funcionário a entrar? ");
		Instalacao i = getInstalacao("Código da instalação onde vai entrar? ");

		consola.println(i.entrar(f) ? "Pode entrar" : "Impedido de entrar!");
		consola.readLine();
	}

	/**
	 * processa a saída de um funcionário
	 */
	private void saidaFuncionario() {
		Funcionario f = getFuncionario("Código do funcionário a sair? ");
		if (f.estaPresente()) {
			consola.println("O funcionário " + f.getNome() + " saiu de " + f.getLocal().getNome());
			f.getLocal().sair(f);
		} else {
			consola.println("O funcionário " + f.getNome() + " não está em nenhuma instalação!");
		}
		consola.readLine();
	}

	/**
	 * pergunta ao utilizador qual o id do funcionário que vai ser processado e
	 * retorna o funcionário correspondente
	 * 
	 * @param msg a mensagem a pedir o funcionário
	 * @return o funcionário pedido
	 */
	private Funcionario getFuncionario(String msg) {
		Funcionario f = null;
		do {
			consola.print(msg);
			int id = consola.readInt();
			f = central.getFuncionario(id);
			if (f == null) {
				consola.println("Esse funcionário não existe!");
				consola.readLine();
			}
		} while (f == null);
		return f;
	}

	/**
	 * pergunta ao utilizador qual o id da instalação que vai ser processada e
	 * retorna a instalação correspondente
	 * 
	 * @param msg a mensagem a pedir a instalação
	 * @return a instalação pedida
	 */
	private Instalacao getInstalacao(String msg) {
		Instalacao i = null;
		do {
			consola.print(msg);
			int id = consola.readInt();
			i = central.getInstalacao(id);
			if (i == null) {
				consola.println("Essa instalação não existe!");
				consola.readLine();
			}
		} while (i == null);
		return i;
	}

	/**
	 * lista todas as presenças em todas as instalações
	 */
	private void listarPresencas() {
		consola.clear();
		for (Instalacao inst : central.getInstalacoes()) {
			consola.println("presentes em " + inst.getNome() + "  (" + inst.getAcesso() + ")");
			for (Funcionario func : inst.getPresentes()) {
				consola.println(func.getNome() + "   (" + func.getAcesso() + ")");
				consola.println("-----------------");

			}
		}
	}
}
