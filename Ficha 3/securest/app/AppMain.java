package securest.app;

import java.util.ArrayList;
import java.util.Arrays;

import p2.tempo.Hora;
import p2.tempo.Horario;
import p2.tempo.Periodo;
import securest.recurso.CentralControlo;
import securest.recurso.Funcionario;
import securest.recurso.Instalacao;

public class AppMain {

	/**
	 * despoleta a aplicação
	 */
	public static void main(String[] args) {

		// criar a central de controlo
		CentralControlo central = new CentralControlo();

		central.addFuncionario(new Funcionario(1, "Asdrúbal", 1));
		central.addFuncionario(new Funcionario(2, "Josefina", 2));
		central.addFuncionario(new Funcionario(3, "Ambrósio", 3));
		central.addFuncionario(new Funcionario(4, "Albertina", 4));
		central.addFuncionario(new Funcionario(5, "Zé Bigboss", 5));
		central.addFuncionario(new Funcionario(6, "Josefa", 3));

		central.addInstalacao(new Instalacao(1, "Sala 1", "", 1,
			new Horario(new ArrayList<Periodo>(Arrays.asList(
				new Periodo(new Hora("08:00:00"), new Hora("20:00:00"))
			))))
		);
		central.addInstalacao(new Instalacao(2, "Sala 2", "", 2,
			new Horario(new ArrayList<Periodo>(Arrays.asList(
				new Periodo(new Hora("08:00:00"), new Hora("12:00:00")),
				new Periodo(new Hora("14:00:00"), new Hora("20:00:00"))
			))))
		);
		central.addInstalacao(new Instalacao(3, "Sala 3", "", 3,
			new Horario(new ArrayList<Periodo>(Arrays.asList(
				new Periodo(new Hora("10:00:00"), new Hora("14:00:00")),
				new Periodo(new Hora("16:00:00"), new Hora("19:00:00"))
			))),
				new ArrayList<Funcionario>(Arrays.asList(
					(Funcionario) central.getFuncionario(1)
				))
			)
		);
		central.addInstalacao(new Instalacao(4, "Laboratório secreto", "", 4,
			new Horario(new ArrayList<Periodo>(Arrays.asList(
				new Periodo(new Hora("00:00:00"), new Hora("07:30:00")),
				new Periodo(new Hora("21:00:00"), new Hora("23:59:00"))
			))),
				new ArrayList<Funcionario>(Arrays.asList(
					(Funcionario) central.getFuncionario(2),
					(Funcionario) central.getFuncionario(3)
				))
			)
		);
		central.addInstalacao(new Instalacao(5, "Jacuzzi", "", 5,
			new Horario(new ArrayList<Periodo>(Arrays.asList(
				new Periodo(new Hora("00:00:00"), new Hora("23:59:00"))
			))),
				new ArrayList<Funcionario>(Arrays.asList(
					(Funcionario) central.getFuncionario(2),
					(Funcionario) central.getFuncionario(4),
					(Funcionario) central.getFuncionario(6)
				))
			)
		);
		central.addInstalacao(new Instalacao(6, "Laboratório 3", "", 3,
			new Horario(new ArrayList<Periodo>(Arrays.asList(
				new Periodo(new Hora("09:30:00"), new Hora("16:30:00")),
				new Periodo(new Hora("17:30:00"), new Hora("20:30:00"))
			))),
				new ArrayList<Funcionario>(Arrays.asList(
					(Funcionario) central.getFuncionario(1)
				))
			)
		);

		// criar a aplicação propriamente dita
		MenuCentral app = new MenuCentral(central);
		app.menuPrincipal();
	}
}
