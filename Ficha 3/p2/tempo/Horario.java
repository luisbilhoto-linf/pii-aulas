package p2.tempo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Horario {
	private List<Periodo> periodos = new ArrayList<Periodo>();

	public Horario(List<Periodo> periodos) {
		this.periodos = periodos;
	}

	public Horario() {
	}

	public List<Periodo> getPeriodos() {
		return Collections.unmodifiableList(periodos);
	}

	public void addPeriodo(Periodo outro) {
		for (int i = periodos.size() - 1; i >= 0; i--) {
			Periodo p = periodos.get(i);
			if (outro.getInicio().compareTo(p.getFim()) > 0) {
				periodos.add(i + 1, outro);
				return;
			}
			if (outro.interseta(p)) {
				outro.junta(p);
				periodos.remove(p);
			}
		}
		periodos.add(0, outro);
	}

	public void removePeriodo(int idx) {
		periodos.remove(idx);
	}

	public boolean estaDentro(Hora h) {
		for (Periodo p : periodos) {
			if (p.estaDentro(h)) {
				return true;
			}
		}
		return false;
	}
}
